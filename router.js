const request = require('request')
var express = require('express');
var router = express.Router();
var apicontroller = require('./apiController/authController');
var passport = require('passport');
var user = require('./Models/users')
var LocalStrategy = require('passport-local').Strategy;
const profile = require('./apiController/profileController')
const teacherSection = require('./apiController/techermoduleController');

const appController = require('./apiController/appController')
const teacherController = require('./apiController/teacherController')
const customerSupportController = require('./apiController/customerSupportController')
const subscriptionController = require('./apiController/subscriptionController')
const dashboardController = require('./apiController/dashboardController')

router.post('/login',
    passport.authenticate('local', {
        failureFlash: 'Username or Password is Incorrect', // req.flash('error','Failed check your credentials');
        failureRedirect: '/pages-login'
    }), function (req, res) { //console.log(req);
        //req.flash('success','you are now Logged In');
        res.locals.user = req.user;
        console.log(req.user, 'jkhakshdkjahskjk');
        res.locals = { title: 'Dashboard' };
        if (req.user.role == 1 || req.user.role == 5)
            res.redirect('/')
        else res.redirect('/logout')

    });

passport.serializeUser(function (user, done) {
    done(null, user.id);
});

passport.deserializeUser(function (id, done) {
    user.getUserById(id, function (err, user) {
        done(err, user);
    });
});
passport.use(new LocalStrategy(function (username, password, done) {
    console.log(username, password, 'username');

    user.getUserByUsername(username, function (err, users) {

        if (err) throw err;
        if (!users) {
            return done(null, false, { message: 'Unknown User..' });
        }
        console.log(users, 'users')
        user.comparePassword(password, users.password, function (err, isMatch) {
            if (err) {
                console.log(err)
                return done(err);
            }
            if (isMatch) {
                console.log(users, 'password matched', password, users.password)
                return done(null, users)
            } else {
                console.log(users, 'password didnt matched', password, users.password)
                return done(null, false, { message: 'Invalid Password' })
            }
        })

    });
}));

router.post('/api/logout', function (req, res) {
    const authHeader = req.headers["authorization"];
    jwt.sign(authHeader, "", { expiresIn: 1 }, (logout, err) => {
        if (logout) {
            res.send({ msg: 'You have been Logged Out' });
        } else {
            res.send({ msg: 'Error' });
        }
    });
})


// Dashboard
router.get('/', requiresLogin, dashboardController.dashboard)

router.get('/profile', requiresLogin, profile.profile);


router.get('/subadmin', requiresLogin, profile.subadminList)

router.get('/login', apicontroller.login);
// router.get('', apicontroller.login, function (req, res) {
//     res.redirect('/pages-login');
// });

function requiresLogin(req, res, next) {
    if (req.user) {
        return next();
    } else {
        res.locals = { title: 'pages-blank' };

        res.redirect('/pages-login');
    }
}
router.get('/logout', function (req, res, next) {
    if (req.session) {
        // delete session object
        req.session.destroy(function (err) {
            if (err) {
                console.log(err, 'hjghgh')
                return next(err);
            } else {
                res.redirect('/pages-login');
            }
        });
    }
});

const multer = require('multer');
var storage = multer.diskStorage({
    destination: function (req, file, cb) {
        cb(null, './upload')
    },
    filename: function (req, file, cb) {
        cb(null, Date.now() + file.originalname);
    }
});
var upload = multer({ storage: storage });

router.post('/register', upload.single('profile'), apicontroller.register);
router.post('/editProfile', requiresLogin, upload.single('picture'), profile.editProfile);
router.post('/saveSubadmin', requiresLogin, upload.single('picture'), profile.addSubadmin)
router.get('/editSubadmin', requiresLogin, profile.editSubadminDetail);
router.post('/saveEditSubadmin', requiresLogin, upload.single('picture'), profile.saveEditSubadmin);
router.get('/permissionUser', requiresLogin, profile.permissionUser);
router.get('/permission:id', requiresLogin, profile.assignRole);
router.get('/subject', requiresLogin, teacherSection.subject)
router.post('/addSubject', requiresLogin, teacherSection.addSubject);
router.get('/deleteSubject:id', requiresLogin, teacherSection.deleteSubject);

router.get('/schoolType', requiresLogin, teacherSection.schoolType);
router.post('/addSchoolType', requiresLogin, teacherSection.addSchoolType)
router.get('/deleteSchoolType:id', requiresLogin, teacherSection.deleteSchoolType)
router.get('/experience', requiresLogin, teacherSection.experience);
router.post('/addExperience', requiresLogin, teacherSection.addExperience);
router.get('/deleteExperience:id', requiresLogin, teacherSection.deleteExperience);
router.get('/country', requiresLogin, teacherSection.country)
router.post('/addCountry', requiresLogin, teacherSection.addCountry);
router.get('/deleteCountry:id', requiresLogin, teacherSection.deleteCountry);
router.get('/state', requiresLogin, teacherSection.state)
router.post('/addState', requiresLogin, teacherSection.addState);
router.get('/deleteState:id', requiresLogin, teacherSection.deleteState);
router.get('/city', requiresLogin, teacherSection.city);
router.post('/addCity', requiresLogin, teacherSection.addCity);
router.get('/deleteCity:id', requiresLogin, teacherSection.deleteCity);
router.get('/hearedFrom', requiresLogin, teacherSection.hearedFrom);
router.post('/addHearedFrom', requiresLogin, teacherSection.addHearedFrom);
router.get('/deleteHearedFrom:id', requiresLogin, teacherSection.deleteHearedFrom);

router.get('/changePassword', requiresLogin, profile.changePasswordPage);
router.post('/changePassword', profile.changePassword);
router.get('/teacherList', requiresLogin, teacherController.teacherList);
router.get('/deleteCandidate/:url', requiresLogin, teacherController.deleteCandidate)
router.get('/newSchool', requiresLogin, teacherController.newSchool)
router.get('/School', requiresLogin, teacherController.School)
router.get('/queryList', requiresLogin, customerSupportController.queryList);
router.get('/feedbackList', requiresLogin, customerSupportController.feedbackList);
router.get('/deleteQuery/:id', requiresLogin, customerSupportController.deleteQuery);
router.get('/deleteFeedback/:id', requiresLogin, customerSupportController.deleteFeedback)
router.get('/approveCandidate', requiresLogin, teacherController.approved)
router.post('/editUser', requiresLogin, upload.fields([{
    name: 'file', maxCount: 1
}, {
    name: 'profile', maxCount: 1
}]), teacherController.editUser);
router.get('/getUserDetail', requiresLogin, teacherController.getUserDetail);
router.post('/editSchool', requiresLogin, upload.single('file'), teacherController.editSchool);
router.post('/addSubscription', subscriptionController.addSubscription);
router.get('/updateUserStatus/:url/:status', requiresLogin, teacherController.updateUserStatus);
router.get('/subscription_list', requiresLogin, subscriptionController.subscription_list);
router.get('/delete_subscription/:id', requiresLogin, subscriptionController.deletePlan);
router.get('/planDetail', requiresLogin, subscriptionController.planDetail);
router.get('/dashboard', requiresLogin, dashboardController.dashboard)
router.get('/deleteSubadmin:id', requiresLogin, profile.deleteSubadmin)
//------------------------ app api-------------------------------------------------------
const jwt = require('jsonwebtoken');
const { route } = require('./Authrouter');
const subscription = require('./Models/subscriptionControl');

function verifyToken(req, res, next) {
    const bearerHeader = req.headers['authorization'];
    console.log(bearerHeader, typeof bearerHeader, req.headers)
    if (typeof bearerHeader !== 'undefined') {
        console.log('hsjj')
        const bearer = bearerHeader.split(' ');
        const beareTOKEN = bearer[1];
        jwt.verify(beareTOKEN, 'secretkey', (err, authData) => {
            if (err) {
                console.log(err)

                res.sendStatus(400)
            }
            else {
                req.auth = authData;
                console.log(req.auth)
                next();
            }
        })

    } else {
        res.sendStatus(403)
    }
}

router.post('/api/userRegister', upload.single('file'), apicontroller.userRegistration);
router.post('/api/userLogin', apicontroller.appLogin);
router.post('/api/test', apicontroller.test);
router.post('/api/subjectList', appController.subjectList);
router.get('/api/classList', appController.classList);
router.get('/api/rolesList', appController.roleList);
router.post('/api/addRole', appController.addRole);
router.post('/api/addClass', appController.addClass);
router.post('/api/sendOtp', appController.sendOtp);
router.post('/api/forgotPassword', appController.changePassword);
router.get('/api/experienceList', appController.experienceList);
router.post('/api/addExperience', appController.addExperience);
router.post('/api/addHereAbout', appController.addHereAbout);
router.get('/api/hearedFromList', appController.hearedFromList);
router.post('/api/schoolType', appController.schoolType);
router.get('/api/schoolTypeList', appController.schoolTypeList);
router.post('/api/getUserDetail', verifyToken, appController.getProfile);
router.post('/api/updateProfile', verifyToken, upload.fields([{
    name: 'file', maxCount: 1
}, {
    name: 'profile', maxCount: 1
}]), appController.updateProfile)
router.get('/api/schoolLists', verifyToken, appController.schoolList);
router.post('/api/verifyAccount', appController.verifyAccount);
router.post('/api/jobPost', verifyToken, upload.single('file'), appController.jobPost);
router.post('/api/postList', verifyToken, appController.postList);
router.post('/api/getPostJobs', verifyToken, appController.getPostJobs);
router.put('/api/editPostJobs/', verifyToken, upload.single('file'), appController.editPostJobs);
router.post('/api/customerSupport', verifyToken, appController.customerSupport);
router.get('/api/custSupportList', appController.custSupportList);
router.post('/api/jobPostApplied', verifyToken, appController.jobPostApplied);
router.post('/api/candidateList', verifyToken, appController.candidateList);
router.post('/api/addFeedback', verifyToken, appController.addFeedback);
router.post('/api/addQuery', verifyToken, appController.addQuery);
router.post('/api/candidateListEligible', verifyToken, appController.candidateListEligible)
router.post('/api/view_job_applied_by_user', verifyToken, appController.view_job_applied_by_user);
router.post('/api/update_applied_job_status', verifyToken, appController.update_applied_job_status);
router.post('/api/view_job_status', verifyToken, appController.view_job_status);
router.post('/api/job_detail', appController.job_detail);
router.post('/api/sendMessage', verifyToken, appController.sendMessage);
router.post('/api/invitationMessageList', verifyToken, appController.invitationMessageList);
router.post('/api/invitationDetail', verifyToken, appController.invitationDetail);
router.get('/api/planList', appController.planList);
router.post('/api/openToWork_status', verifyToken, appController.openToWork_status);
router.get('/api/teacher_list_searching_for_job', verifyToken, appController.teacher_list_searching_for_job);
router.get('/api/customer_support', verifyToken, appController.customerSupport_app);
router.post('/api/query_list_user', verifyToken, appController.query_list_user);
router.post('/api/view_mail', verifyToken, appController.view_mail);
router.post('/api/notification_list', appController.notification_list);
router.post('/api/jobApplied_listing', verifyToken, appController.jobApplied_listing);
router.post('/api/addUserPlan', appController.addUserPlan);
router.post('/api/userPlanList',verifyToken, appController.userPlanList);


router.get('/api/countryList', appController.countryList);
router.post('/api/stateList', appController.stateList);
router.post('/api/cityList', appController.cityList);
router.post('/api/fcmList', appController.fcmList);

router.post('/api/testinsert', appController.testinsert);

router.get('*', requiresLogin, apicontroller.error);

router.post('/api/teacherList', appController.teacherList);

// router.post('/api/send_fcm', appController.send_fcm);

router.post('/api/testFcm', appController.testFcm);


//M-pesa Payment Getway
const urls = {
    'stk': "https://sandbox.safaricom.co.ke/mpesa/stkpush/v1/processrequest",
    "simulate": "https://sandbox.safaricom.co.ke/mpesa/c2b/v1/simulate",
    "b2c": "https://sandbox.safaricom.co.ke/mpesa/b2c/v1/paymentrequest",
    "base_url": "http://182.76.237.236:3000"
}
const maker = access_token()
const headers = {
    "Authorization": "Bearer " + maker
}


router.post('/testmpasa', (req, res) => {
    let date = new Date()
    let timestamp = date.getDate() + "" + "" + date.getMonth() + "" + "" + date.getFullYear() + "" + "" + date.getHours() + "" + "" + date.getMinutes() + "" + "" + date.getSeconds()

    res.status(200).json({ message: "We're up and running. Happy Coding", time: new Buffer.from(timestamp).toString('base64'), token: headers })
})

router.post('/access_token', access, (req, res) => {
    res.status(200).json({ access_token: req.access_token });
})

router.post('/registermpesa', access, (req, resp) => {
    let url = "https://sandbox.safaricom.co.ke/mpesa/c2b/v1/registerurl"
    let auth = "Bearer " + req.access_token

    request(
        {
            url: url,
            method: "POST",
            headers: {
                "Authorization": auth
            },
            json: {
                "ShortCode": "603085",
                "ResponseType": "Complete",
                "ConfirmationURL": "http://182.76.237.236:3000/confirmation",
                "ValidationURL": "http://182.76.237.236:3000/validation"
            }
        },
        function (error, response, body) {
            if (error) { console.log(error) }
            resp.status(200).json(body)
        }
    )
})

router.post('/confirmation', (req, res) => {
    console.log('....................... confirmation .............')
    console.log(req.body)
})

router.post('/validation', (req, resp) => {
    console.log('....................... validation .............')
    console.log(req.body)
})


router.post('/simulate', access, (req, res) => {
    let url = "https://sandbox.safaricom.co.ke/mpesa/c2b/v1/simulate"
    let auth = "Bearer " + req.access_token

    request(
        {
            url: url,
            method: "POST",
            headers: {
                "Authorization": auth
            },
            json: {
                "ShortCode": "603085",
                "CommandID": "CustomerPayBillOnline",
                "Amount": "100",
                "Msisdn": "254708374149",
                "BillRefNumber": "TestAPI"
                // "ShortCode": "174379",
                // "CommandID": "CustomerPayBillOnline",
                // "Amount": "100",
                // "Msisdn": "254708374149",
                // "BillRefNumber": "TestAPI"
            }
        },
        function (error, response, body) {
            if (error) {
                console.log(error)
            }
            else {
                res.status(200).json(body)
            }
        }
    )
})

router.post('/balance', access, (req, resp) => {
    let url = "https://sandbox.safaricom.co.ke/mpesa/accountbalance/v1/query"
    let auth = "Bearer " + req.access_token

    request(
        {
            url: url,
            method: "POST",
            headers: {
                "Authorization": auth
            },
            json: {
                "Initiator": "apitest342",
                "SecurityCredential": "5PW7nppk",
                "CommandID": "AccountBalance",
                "PartyA": "603085",
                "IdentifierType": "4",
                "Remarks": "bal",
                "QueueTimeOutURL": "http://182.76.237.236:3000/bal_timeout",
                "ResultURL": "http://182.76.237.236:3000/bal_result"
            }
        },
        function (error, response, body) {
            if (error) {
                console.log(error)
            }
            else {
                resp.status(200).json(body)
            }
        }
    )
})

router.post('/transactionstatus', access, (req, resp) => {
    let url = "https://sandbox.safaricom.co.ke/mpesa/transactionstatus/v1/query"
    let auth = "Bearer " + req.access_token

    request(
        {
            url: url,
            method: "POST",
            headers: {
                "Authorization": auth
            },
            json: {
                "Initiator": "apitest342",
                "SecurityCredential": "5PW7nppk",
                "CommandID": "TransactionStatusQuery",
                "TransactionID": "OEI2AK4Q16",
                "PartyA": "603085",
                "IdentifierType": "4",
                "Remarks": "bal",
                "Occassion": "asas",
                "QueueTimeOutURL": "http://182.76.237.236:3000/timeout",
                "ResultURL": "http://182.76.237.236:3000/result"
            }
        },
        function (error, response, body) {
            if (error) {
                console.log(error)
            }
            else {
                resp.status(200).json(body)
            }
        }
    )
})

router.post('/stk', access, (req, res) => {
    const url = "https://sandbox.safaricom.co.ke/mpesa/stkpush/v1/processrequest",
        auth = "Bearer " + req.access_token
    console.log(auth);
    let date = new Date();
    const timestamp = `${date.getFullYear() + ("0" + (date.getMonth() + 1)).slice(-2) + ("0" + date.getDate()).slice(-2) + ("0" + date.getHours() ).slice(-2) + ("0" + date.getMinutes()).slice(-2) + ("0" + date.getSeconds()).slice(-2)}`;
    // let date = new Date()
    // const timestamp = `${date.getFullYear()}${date.getMonth() + 1}${date.getDate()}${date.getHours()}${date.getMinutes()}${date.getSeconds()}`
    const password = new Buffer.from('174379' + 'bfb279f9aa9bdbcf158e97dd71a467cd2e0c893059b10f78e6b72ada1ed2c919' + timestamp).toString('base64')
    console.log({
        "BusinessShortCode": "174379",
        "Password": password,
        "Timestamp": timestamp,
        "TransactionType": "CustomerPayBillOnline",
        "Amount": "1",
        "PartyA": "254708374149",
        "PartyB": "174379",
        "PhoneNumber": "254708374149",
        "CallBackURL": "http://182.76.237.237:9000/stk_callback",
        "AccountReference": "Test",
        "TransactionDesc": "TestPay"
    })
    request(
        {
            url: url,
            method: "POST",
            headers: {
                "Authorization": auth
            },
            json: {
                "BusinessShortCode": "174379",
                "Password": password,
                "Timestamp": timestamp,
                "TransactionType": "CustomerPayBillOnline",
                "Amount": "1",
                "PartyA": "254716437799",
                "PartyB": "174379",
                "PhoneNumber": "254716437799",
                "CallBackURL": "https://www.ionicclouds.com",
                "AccountReference": "Test",
                "TransactionDesc": "TestPay"
            }
        },
        function (error, response, body) {
            if (error) {
                console.log(error)
            }
            else {
                res.status(200).json(body)
            }
        }
    )
})

router.get('/b2c/:id/:role', access, async (req, res) => {
    var role = req.params.role;
    var booking_id = req.params.id;
    const url = 'https://sandbox.safaricom.co.ke/mpesa/b2c/v1/paymentrequest',
        auth = 'Bearer ' + req.access_token
    console.log(auth, 'auth33333#######')
    if (role == 2) {
        var bookedGasDetail = await bookGas.find({ _id: mongoose.Types.ObjectId(booking_id) }).populate('venderId').exec();

        var priceToPay = bookedGasDetail[0].vendorPrice;

    }
    else {
        var bookedGasDetail = await bookGas.find({ _id: mongoose.Types.ObjectId(booking_id) }).populate('deliveryBoyId').exec();


        var priceToPay = bookedGasDetail[0].rider_price_pay;

    }

    request({
        method: "POST",
        url: url,
        headers: {
            "Authorization": auth
        },
        json:
        {
            "InitiatorName": "apiop37",
            "SecurityCredential": "jUb+dOXJiBDui8FnruaFckZJQup3kmmCH5XJ4NY/Oo3KaUTmJbxUiVgzBjqdL533u5Q435MT2VJwr/cJ1D2zJ7M51JvuKfkXBnkx550Jlm06Gx9c36doviHv29gpaCaXAiykifS7464uJWO8mJJuGQNOZOU4q4jMrHsGldYPJZ5GGkontlvLzrte0Cw8kq0XP5um9I8aQXqgDfZaNnRFAZGFWxQXKN6IR6IfOuFwMVRFmbnXis8uGyk7oMv6AVCCgnPer0176S1A4itPqxFWZnvwmW9c1oWAmDrx6oQqNAH76OrCiMvjH1peeMHAQNxC45GPYCKfsdB0TzR/1fuZvA==", // raw_password: Safaricom3021#
            "CommandID": "BusinessPayment",
            "Amount": 10,
            "PartyA": 603021,
            "PartyB": 254708374149,
            "Remarks": "Test",
            "QueueTimeOutURL": "https://675b-182-76-237-226.ngrok.io/b2c_timeout_url",
            "ResultURL": `https://675b-182-76-237-226.ngrok.io/b2c_result_url/${role}/${booking_id}`,
            "Occassion": "null"
        }

    },
        async function (error, response, body) {
            if (error) {
                console.log(error)
                req.flash('error', JSON.stringify(error))
                res.redirect('/sysadmin/completedOrder')
            }
            else {
                console.log(response.body, 'responserfg');
                var responsesPayment = response.body;

                var json = {
                    bookedGasDetail: bookedGasDetail[0],
                    responsesPayment,
                    role: role

                }
                req.flash('success', 'processing Request');
                console.log(json);
                // res.redirect(`/sysadmin/payment_status/${response.body.ConversationID}`)
                res.render(`sysadmin/paymentAdmin`, { json: [json] })
            }
        }

    )
})



router.get('/reverse', access, (req, res) => {
    const url = 'https://sandbox.safaricom.co.ke/mpesa/reversal/v1/request',
        auth = 'Bearer ' + req.access_token

    request({
        method: "POST",
        url: url,
        headers: {
            "Authorization": auth
        },
        json: {
            "Initiator": "apitest342",
            "SecurityCredential": "Q9KEnwDV/V1LmUrZHNunN40AwAw30jHMfpdTACiV9j+JofwZu0G5qrcPzxul+6nocE++U6ghFEL0E/5z/JNTWZ/pD9oAxCxOik/98IYPp+elSMMO/c/370Joh2XwkYCO5Za9dytVmlapmha5JzanJrqtFX8Vez5nDBC4LEjmgwa/+5MvL+WEBzjV4I6GNeP6hz23J+H43TjTTboeyg8JluL9myaGz68dWM7dCyd5/1QY0BqEiQSQF/W6UrXbOcK9Ac65V0+1+ptQJvreQznAosCjyUjACj35e890toDeq37RFeinM3++VFJqeD5bf5mx5FoJI/Ps0MlydwEeMo/InA==",
            "CommandID": "TransactionReversal",
            "TransactionID": "NLJ11HAY8V",
            "Amount": "100",
            "ReceiverParty": "601342",
            "RecieverIdentifierType": "11",
            "ResultURL": "http://182.76.237.236:3000/reverse_result_url",
            "QueueTimeOutURL": "http://182.76.237.236:3000/reverse_timeout_url",
            "Remarks": "Wrong Num",
            "Occasion": "sent wrongly"
        }
    },
        function (error, response, body) {
            if (error) {
                console.log(error)
                req.flash('error', error)
                res.redirect('/sysadmin/completedOrder')
            }
            else {
                req.flash('success', success)
                res.redirect('/sysadmin/completedOrder')
            }
        }
    )
})

router.post('/reverse_result_url', (req, res) => {
    console.log("--------------------Reverse Result -----------------")
    console.log(JSON.stringify(req.body.Result.ResultParameters))
})

router.post('/reverse_timeout_url', (req, _res) => {
    console.log("-------------------- Reverse Timeout -----------------")
    console.log(req.body)
})

router.post('/b2c_result_url/:role/:id', async (req, res) => {
    console.log("-------------------- B2C Result -----------------", req.body.Result)
    var getDetailsHere = await salaryRequest.find({ ConversationID: req.body.Result.ConversationID }).exec()
    console.log(getDetailsHere);
    if (req.body.Result.ResultCode == '1') {

        var userVariable = {
            "resultDesc": req.body.Result.ResultDesc,
            "OriginatorConversationID": req.body.Result.OriginatorConversationID,
            "ConversationID": req.body.Result.ConversationID,
            "TransactionID": req.body.Result.TransactionID,
            'status': 'pending'
        }
        try {
            var saveSalaryRequest = new salaryRequest(userVariable);
            await saveSalaryRequest.save();
            return res.send({
                success: true,
                ResultCode: req.body.Result.ResultCode
            });
        }
        catch (err) {
            console.log(err)
        }

    }
    else {
        var userVariable = {
            resultDesc: req.body.Result.ResultDesc,
            OriginatorConversationID: req.body.Result.OriginatorConversationID,
            ConversationID: req.body.Result.ConversationID,
            TransactionID: req.body.Result.TransactionID,
            status: 'completed'
        }
        try {
            var saveSalaryRequest = new salaryRequest(userVariable);
            await saveSalaryRequest.save();
            if (req.params.role == 2) {
                await bookGas.updateOne({ _id: req.params.id }, { $set: { vendor_payment_status: 'completed' } });

            }
            else {
                await bookGas.updateOne({ _id: req.params.id }, { $set: { rider_payment_status: 'completed' } });

            }
            return res.send({
                success: true,
                message: 'hi'
            });
        }
        catch (err) {
            console.log(err);
        }
    }

})

router.post('/b2c_timeout_url', (req, res) => {
    console.log("-------------------- B2C Timeout -----------------")
    console.log(req.body)
})

router.post('/stk_callback', (req, res) => {
    console.log('.......... STK Callback ..................')
    // console.log(JSON.stringify(req.body.Body.stkCallback))
})

router.post('/bal_result', (req, resp) => {
    console.log('.......... Account Balance ..................')
    console.log(req.body)
})

router.post('/bal_timeout', (req, resp) => {
    console.log('.......... Timeout..................')
    console.log(req.body)
})

function access(req, res, next) {
    // access token
    let url = "https://sandbox.safaricom.co.ke/oauth/v1/generate?grant_type=client_credentials"
    let auth = new Buffer.from("yJIvh197BsT0KuUyzY8zEoCATp84Wxxj:8KyZBpTlvzQzKBlA").toString('base64');

    request(
        {
            url: url,
            headers: {
                "Authorization": "Basic " + auth
            }
        },
        (error, response, body) => {
            if (error) {
                console.log(error)
            }
            else {
                // let resp =
                req.access_token = JSON.parse(body).access_token
                // console.log(req.access_token)
                next()
            }
        }
    )
}


function access_token() {
    // access token
    let url = "https://sandbox.safaricom.co.ke/oauth/v1/generate?grant_type=client_credentials"
    let auth = new Buffer.from("yJIvh197BsT0KuUyzY8zEoCATp84Wxxj:8KyZBpTlvzQzKBlA").toString('base64');

    request(
        {
            url: url,
            headers: {
                "Authorization": "Basic " + auth
            }
        },
        (error, response, body) => {
            if (error) {
                console.log(error)
            }
            else {
                // let resp =
                console.log(body);
                return JSON.parse(body).access_token;
            }
        }
    )
}

module.exports = router;
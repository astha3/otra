const mongoose = require('mongoose');
const user = require('../Models/users');
const subject = require('../Models/subject');
const classes = require('../Models/class');
const bcrypt = require('bcrypt-nodejs');
const roles = require('../Models/roles');
const experience = require('../Models/experience');
const hearedFrom = require('../Models/hearedFrom');
const schoolType = require('../Models/schoolType');
const jobModel = require('../Models/jobPost');
const jobAppliedModel = require('../Models/jobApplied');
const supportModel = require('../Models/customerSupport');
const query = require('../Models/query');
const feedback = require('../Models/feedback');
const job_statuses = require('../Models/job_statuses');
const invitationMessage = require('../Models/invitationMessage');
const applied_for_job = require('../Models/jobApplied');
const jobPost = require('../Models/jobPost');
const subscription = require('../Models/subscriptionControl');
const jobs = require('../Models/jobPost');
const notifications = require('../Models/notificationList');
const teacher = require('../Models/teacher');
const experiences = require('../Models/experience');
const { json } = require('body-parser');
const notificationList = require('../Models/notificationList');
const country = require('../Models/country');
const state = require('../Models/state');
const city = require('../Models/city');
const userPlanModel = require('../Models/userPlan');
const fcmModel = require('../Models/fcm');
const login_logModel = require('../Models/login_log');


var apn = require('apn');
var join = require('path').join,
    pfx = join(__dirname, './AuthKey_5KC556Q4ZY.p8');
var options = {
    token: {
        key: pfx,
        keyId: "5KC556Q4ZY",
        teamId: "727K423CVY"
    },
    production: false
};

var apnProvider = new apn.Provider(options);
var nodemailer = require('nodemailer');

var SALT_FACTOR = 10;

const url = {
    base_url: 'http://182.76.237.237:9000/upload'
};
var noop = function () { };

exports.teacherList = async (req, res) => {
    try {
        var teacherList = await teacher.aggregate([


            {
                "$project": {
                    "teacher": "$$ROOT"
                }
            },
            {
                "$sort": {
                    "createdAt": -1
                }
            },
            {
                $lookup: {
                    from: "users",
                    localField: "teacher.teacher_id",
                    foreignField: "_id",
                    as: "user"
                }
            },
            {
                "$unwind": {
                    "path": "$user",
                    "preserveNullAndEmptyArrays": true
                }
            },
            {
                $lookup: {
                    from: "school_types",
                    localField: "teacher.school_type",
                    foreignField: "_id",
                    as: "school"
                }
            },
            {
                "$unwind": {
                    "path": "$school",
                    "preserveNullAndEmptyArrays": true
                }
            },
            {
                $lookup: {
                    from: "experiences",
                    localField: "teacher.experience",
                    foreignField: "_id",
                    as: "experience"
                }
            },
            {
                "$unwind": {
                    "path": "$experience",
                    "preserveNullAndEmptyArrays": true
                }
            },
            {
                $lookup: {
                    from: "countries",
                    localField: "user.country",
                    foreignField: "_id",
                    as: "country"
                }
            },
            {
                "$unwind": {
                    "path": "$country",
                    "preserveNullAndEmptyArrays": true
                }
            },
            {
                $lookup: {
                    from: "states",
                    localField: "user.state",
                    foreignField: "_id",
                    as: "state"
                }
            },
            {
                "$unwind": {
                    "path": "$state",
                    "preserveNullAndEmptyArrays": true
                }
            },
            {
                $lookup: {
                    from: "cities",
                    localField: "user.city",
                    foreignField: "_id",
                    as: "city"
                }
            },
            {
                "$unwind": {
                    "path": "$city",
                    "preserveNullAndEmptyArrays": true
                }
            },
            {
                "$project": {
                    "resume": "$teacher.resume",
                    "subject": "$teacher.subject",
                    "educationDetail": "$teacher.educationDetail",
                    "username": "$user.username",
                    "country": "$country.name",
                    "state": "$state.name",
                    "city": "$city.name",
                    "email": "$user.email",
                    "phoneNumber": "$user.phoneNumber",
                    "profileImage": "$user.profileImage",
                    "address": "$user.address",
                    "schoolType": "$school.type",
                    "experience": "$experience.experience"
                }
            }
        ])
        let JsonReversedArray = teacherList.reverse();
        //  console.log(teacherList, "teacherList")
        return res.send({ 'status': 'true', msg: "Details have been fetched successfully.", data: JsonReversedArray })
    } catch (error) {
        res.status(200).json({ 'status': 'error', 'msg': 'something went wrong', 'data': [] });
    }
}


exports.subjectList = function (req, res, next) {
    var school_type = req.body.school_type;
    if (school_type) {
        subject.find({ school_type: school_type }).exec().then(data => {
            if (data.length > 0) {
                res.status(200).json({ 'status': 'success', 'msg': 'subject list ', 'data': data });
            }
            else {
                res.status(200).json({ 'status': 'error', 'msg': 'no data available ', 'data': [] });

            }
        })
    } else {
        res.status(200).json({ 'status': 'error', 'msg': 'school type field is required ', 'data': [] });
    }
}

exports.countryList = function (req, res, next) {
    try {
        country.find().exec().then(data => {
            if (data.length > 0) {
                res.status(200).json({ 'status': 'success', 'msg': 'Country list found successfully', 'data': data });
            }
            else {
                res.status(200).json({ 'status': 'error', 'msg': 'No data available ', 'data': [] });

            }
        })
    } catch (error) {
        res.status(200).json({ 'status': 'error', 'msg': 'something went wrong', 'data': [] });
    }
}
exports.stateList = function (req, res, next) {
    try {
        state.find({ country_id: req.body.country_id }).exec().then(data => {
            if (data.length > 0) {
                res.status(200).json({ 'status': 'success', 'msg': 'State list found successfully', 'data': data });
            }
            else {
                res.status(200).json({ 'status': 'error', 'msg': 'No data available ', 'data': [] });

            }
        })
    } catch (error) {
        res.status(200).json({ 'status': 'error', 'msg': 'something went wrong', 'data': [] });
    }
}

exports.cityList = function (req, res, next) {
    try {
        city.find({ state_id: req.body.state_id }).exec().then(data => {
            if (data.length > 0) {
                res.status(200).json({ 'status': 'success', 'msg': 'City list found successfully', 'data': data });
            }
            else {
                res.status(200).json({ 'status': 'error', 'msg': 'No data available ', 'data': [] });

            }
        })
    } catch (error) {
        res.status(200).json({ 'status': 'error', 'msg': 'something went wrong', 'data': [] });
    }
}

exports.testinsert = function (req, res) {
    var userVariable = {
        name: req.body.name,
        state_id: mongoose.Types.ObjectId(req.body.state_id),
        status: req.body.status
    }

    new city(userVariable).save().then(data => {
        res.status(200).json({ 'status': 'success', 'msg': 'Add data Successfully', 'data': data });
    }).catch((error) => {
        console.log(error)
        res.status(200).json({ 'status': 'error', 'msg': 'something went wrong', 'data': [] });
    });
}
exports.addClass = function (req, res, next) {
    let uservariable = {
        classname: req.body.class
    }
    let addClass = new classes(uservariable);
    addClass.save().then(doc => {
        res.status(200).json({ 'status': 'success', 'msg': 'subject list ', 'data': doc });
    })
}

exports.classList = function (req, res, next) {
    classes.find().exec().then(data => {
        if (data.length > 0) {
            res.status(200).json({ 'status': 'success', 'msg': 'subject list ', 'data': data });
        }
        else {
            res.status(200).json({ 'status': 'error', 'msg': 'bo data available ', 'data': [] });

        }
    })
}

exports.addRole = function (req, res, next) {
    var uservariable = {
        rolename: req.body.role
    }
    try {
        let addRole = new roles(uservariable);

        addRole.save().then(doc => {
            res.status(200).json({ 'status': 'success', 'msg': 'add role', 'data': doc });
        })
    }
    catch (err) {
        res.status(200).json({ 'status': 'success', 'msg': 'something went wrong', 'data': [] });
    }
}

exports.roleList = function (req, res, next) {
    roles.find().exec().then(data => {
        if (data.length > 0) {
            res.status(200).json({ 'status': 'success', 'msg': 'roles list ', 'data': data });
        }
        else {
            res.status(200).json({ 'status': 'error', 'msg': 'no data available ', 'data': [] });

        }
    })
}

exports.addExperience = function (req, res, next) {
    var uservariable = {
        experience: req.body.experience
    }

    let addExperience = new experience(uservariable);

    addExperience.save(function (err, doc) {
        if (err) {

            res.status(200).json({ 'status': 'error', 'msg': 'something went wrong', 'data': doc });

        }
        else {
            res.status(200).json({ 'status': 'success', 'msg': 'added Experience', 'data': doc });
        }
    })
}

exports.experienceList = function (req, res, next) {
    experience.find().exec(function (err, data) {
        if (err) {
            res.status(200).json({ 'status': 'error', 'msg': 'no data available ', 'data': [] });

        }
        else {
            if (data.length > 0) {
                res.status(200).json({ 'status': 'success', 'msg': 'experience list ', 'data': data });
            }
            else {
                res.status(200).json({ 'status': 'error', 'msg': 'no data available ', 'data': [] });
            }
        }
    })
}

function generateOTP() {
    var digits = '0123456789';
    let OTP = "";
    for (let i = 0; i <= 3; i++) {
        OTP += digits[Math.floor(Math.random() * 10)];
    }
    return Number(OTP);
}

function sendEmail(email, otp) {
    var text = `Dear user, your otp for verification is ${otp}`
    var transporter = nodemailer.createTransport({
        service: 'gmail',
        auth: {
            user: 'maxtratechnologies.test@gmail.com',
            pass: 'Maxtra@123'
        }
    });

    var mailOptions = {
        from: 'OTRA maxtratechnologies.test@gmail.com',
        to: email,
        subject: 'otp verification',
        text: text
    };

    transporter.sendMail(mailOptions, function (error, info) {
        if (error) {
            console.log(error, null);
        } else {
            console.log(null, info);
        }
    })
}

exports.sendOtp = function (req, res, next) {
    var username = req.body.username
    if (isNaN(username)) {
        var search = { email: username };
    } else {
        var search = { phoneNumber: username };
    }

    user.find(search)
        .exec(function (err, userData) {
            console.log(userData)
            if (err) {
                res.status(500).send({ 'status': 'error', 'msg': 'Something want worng', 'data': null });
            } else {
                if (userData.length > 0) {
                    let otp = generateOTP();


                    res.status(200).send({ 'status': 'success', 'msg': 'successfully sent OTP', 'data': { otp } });
                } else {
                    res.status(200).send({ 'status': 'error', 'msg': 'user not exist', 'data': null });

                }
            }
        })
}

exports.verifyAccount = function (req, res, next) {
    let email = req.body.username;
    let phoneNumber = req.body.number
    user.count({ $or: [{ email }, { phoneNumber }] })
        .exec(function (err, userData) {
            if (err) {
                res.status(500).send({ 'status': 'error', 'msg': 'Something went worng', 'data': [] });
            } else {
                if (userData > 0) {
                    res.status(200).json({ 'status': 'error', 'msg': 'email or number already exist', 'data': [] });

                } else {
                    let otp = generateOTP();
                    var upddata = {
                        'otp': otp,
                        'username': req.body.username,
                    }

                    var email = req.body.username

                    sendEmail(email, otp, (emailErr, emailRes) => {
                        if (emailErr) {
                            return res.send({ responseCode: 500, responseMessage: "Internal server error1.", responseResult: emailErr });
                        } else {
                        }
                    })
                    res.status(200).json({ 'status': 'success', 'msg': 'otp sent successfully', 'data': upddata });

                }

            }
        });
}

exports.changePassword = function (req, res, next) {
    let username = req.body.username
    if (isNaN(username)) {
        var search = { email: username };
    } else {
        var search = { phoneNumber: username };
    }
    user.find(search).find(function (err, data) {

        if (err) {
            res.status(500).send({ 'status': 'error', 'msg': 'something went wrong', 'data': [] });

        }
        if (data.length > 0) {
            let old_password = req.body.old_password;
            if (old_password) {
                user.comparePassword(req.body.old_password, data[0].password, function (err, isMatch) {
                    if (isMatch) {
                        bcrypt.genSalt(SALT_FACTOR, function (err, salt) {
                            if (err) {
                                return done(err);
                            }
                            bcrypt.hash(req.body.password, salt, noop, function (err, hashedPassword) {

                                if (err) { return done(err); }

                                let password = hashedPassword;
                                var Variables = {
                                    password: password,
                                };
                                user.update({ _id: mongoose.Types.ObjectId(data[0]._id) }, Variables, function (err, raw) {
                                    if (err) {
                                        res.status(200).send({ 'status': 'success', 'msg': 'something went wrong', 'data': [] });

                                    }
                                    else {
                                        res.status(200).send({ 'status': 'success', 'msg': 'password changed successfully', 'data': [] });
                                    }
                                })
                            });
                        });
                    }
                    else {
                        res.status(200).send({ 'status': 'error', 'msg': 'incorrect old_password ', 'data': [] });
                    }
                })
            }
            else {
                bcrypt.genSalt(SALT_FACTOR, function (err, salt) {
                    if (err) {
                        console.log(err);
                        return done(err);
                    }
                    bcrypt.hash(req.body.newpassword, salt, noop, function (err, hashedPassword) {
                        if (err) { return done(err); }

                        let password = hashedPassword;
                        var Variables = {
                            password: password,
                        };
                        user.update({ _id: mongoose.Types.ObjectId(data[0]._id) }, Variables, function (err, raw) {
                            if (err) {
                                res.status(200).send({ 'status': 'success', 'msg': 'something went wrong', 'data': [] });

                            }
                            else {
                                res.status(200).send({ 'status': 'success', 'msg': 'password changed successfully', 'data': [] });
                            }
                        })
                    });
                });
            }
        }
        else {
            res.status(200).send({ 'status': 'error', 'msg': 'not exist ', 'data': [] });

        }

    })

}

exports.addHereAbout = function (req, res, next) {
    let hearedFromAdded = new hearedFrom({ hearedFrom: req.body.hearedFrom });
    hearedFromAdded.save().then(function (err, data) {
        if (err) {
            console.log(err);
        }
        res.status(200).json({ 'status': 'success', 'msg': 'added succesfuly ', 'data': data });
    })

}

exports.hearedFromList = function (req, res, next) {
    hearedFrom.find({}).sort({ 'createdAt': -1 }).exec(function (err, data) {
        if (err) {
            res.status(200).json({ 'status': 'success', 'msg': 'something went wrong ', 'data': [] });

        }
        else {
            res.status(200).json({ 'status': 'success', 'msg': 'list heared about us', 'data': data });

        }
    })
}

exports.schoolType = function (req, res, next) {
    let schoolTypeadd = new schoolType({ type: req.body.type });
    schoolTypeadd.save().then(function (err, data) {
        if (err) {
            console.log(err);
        }
        res.status(200).json({ 'status': 'success', 'msg': 'added succesfuly ', 'data': data });
    })

}

exports.schoolTypeList = function (req, res, next) {
    schoolType.find({}).sort({ 'createdAt': 1 }).exec(function (err, data) {
        if (err) {
            res.status(200).json({ 'status': 'success', 'msg': 'something went wrong ', 'data': [] });

        }
        else {
            res.status(200).json({ 'status': 'success', 'msg': 'school type list', 'data': data });

        }
    })
}



exports.getProfile = async function (req, res, next) {
    if (req.body.role == 3) {
        var getAllData = await user.aggregate([
            {
                "$match": {
                    _id: mongoose.Types.ObjectId(req.body.id)
                }
            },
            {
                "$project": {
                    "ca": "$$ROOT"
                }
            },
            {
                $lookup: {
                    from: "teachers",
                    localField: "ca._id",
                    foreignField: "teacher_id",
                    as: "teacher"
                }
            },
            {
                "$unwind": {
                    "path": "$teacher",
                    "preserveNullAndEmptyArrays": true
                }
            },
            {
                $lookup: {
                    from: "school_types",
                    localField: "teacher.school_type",
                    foreignField: "_id",
                    as: "school"
                }
            },
            {
                "$unwind": {
                    "path": "$school",
                    "preserveNullAndEmptyArrays": true
                }
            },
            {
                $lookup: {
                    from: "subjects",
                    localField: "teacher.subject",
                    foreignField: "_id",
                    as: "subject"
                }
            },
            {
                "$unwind": {
                    "path": "$subject",
                    "preserveNullAndEmptyArrays": true
                }
            },
            {
                $lookup: {
                    from: "experiences",
                    localField: "teacher.experience",
                    foreignField: "_id",
                    as: "experience"
                }
            },
            {
                "$unwind": {
                    "path": "$experience",
                    "preserveNullAndEmptyArrays": true
                }
            },
            {
                $lookup: {
                    from: "countries",
                    localField: "ca.country",
                    foreignField: "_id",
                    as: "co"
                }
            },
            {
                "$unwind": {
                    "path": "$co",
                    "preserveNullAndEmptyArrays": true
                }
            },
            {
                $lookup: {
                    from: "states",
                    localField: "ca.state",
                    foreignField: "_id",
                    as: "st"
                }
            },
            {
                "$unwind": {
                    "path": "$st",
                    "preserveNullAndEmptyArrays": true
                }
            },
            {
                $lookup: {
                    from: "cities",
                    localField: "ca.city",
                    foreignField: "_id",
                    as: "ct"
                }
            },
            {
                "$unwind": {
                    "path": "$ct",
                    "preserveNullAndEmptyArrays": true
                }
            },
            {
                "$project": {
                    "teacher_id": "$teacher.teacher_id",
                    "username": "$ca.username",
                    "email": "$ca.email",
                    "phoneNumber": "$ca.phoneNumber",
                    "countrycode": "$ca.country_code",
                    "address": "$ca.address",
                    "school_type_id": "$school._id",
                    "school_type_name": "$school.type",
                    "subject_id": "$subject._id",
                    "skills": "$teacher.subject",
                    "school_type": "$teacher.school_type",
                    "resume": "$teacher.resume",
                    "profileImage": "$ca.profileImage",
                    "experience_id": "$experience._id",
                    "experience": "$experience.experience",
                    // "profilePic": "$ca.profilePic",
                    "educationDetails": "$teacher.education_detail",
                    "country": "$co.name",
                    "state": "$st.name",
                    "city": "$ct.name",
                    "country_id": "$ca.country",
                    "state_id": "$ca.state",
                    "city_id": "$ca.city"
                }
            }
        ])

        if (getAllData.length > 0) {
            if (getAllData[0].resume != null) {
                getAllData[0].resume = `${url.base_url}/${getAllData[0].resume}`
            }
            if (getAllData[0].profileImage != null) {
                getAllData[0].profileImage = `${url.base_url}/${getAllData[0].profileImage}`
            }
            res.status(200).json({ 'status': 'success', 'msg': 'get requested data', 'data': getAllData });
        }
        else {
            res.status(200).json({ 'status': 'success', 'msg': 'user not exist', 'data': [] });

        }
    }
    else {

        var getAllData = await user.aggregate([
            {
                "$match": {
                    _id: mongoose.Types.ObjectId(req.body.id)
                }
            },
            {
                "$project": {
                    "ca": "$$ROOT"
                }
            },
            {
                $lookup: {
                    from: "countries",
                    localField: "ca.country",
                    foreignField: "_id",
                    as: "co"
                }
            },
            {
                "$unwind": {
                    "path": "$co",
                    "preserveNullAndEmptyArrays": true
                }
            },
            {
                $lookup: {
                    from: "states",
                    localField: "ca.state",
                    foreignField: "_id",
                    as: "st"
                }
            },
            {
                "$unwind": {
                    "path": "$st",
                    "preserveNullAndEmptyArrays": true
                }
            },
            {
                $lookup: {
                    from: "cities",
                    localField: "ca.city",
                    foreignField: "_id",
                    as: "ct"
                }
            },
            {
                "$unwind": {
                    "path": "$ct",
                    "preserveNullAndEmptyArrays": true
                }
            },
            {
                "$project": {
                    "username": "$ca.username",
                    "email": "$ca.email",
                    "phoneNumber": "$ca.phoneNumber",
                    "schoolNumber": "$ca.schoolNumber",
                    "know_about_us": "$ca.know_about_us",
                    "countrycode": "$ca.country_code",
                    "address": "$ca.address",
                    "role": "$ca.role",
                    "status": "$ca.status",
                    "approve": "$ca.approve",
                    "profileImage": "$ca.profileImage",
                    "country": "$co.name",
                    "state": "$st.name",
                    "city": "$ct.name",
                    "country_id": "$ca.country",
                    "state_id": "$ca.state",
                    "city_id": "$ca.city"
                }
            }
        ])

        if (getAllData.length > 0) {
            if (getAllData[0].profileImage != null) {
                getAllData[0].profileImage = `${url.base_url}/${getAllData[0].profileImage}`
            }
            res.status(200).json({ 'status': 'success', 'msg': 'user detail', 'data': getAllData });
        }
        else {
            res.status(200).json({ 'status': 'success', 'msg': 'user not exist', 'data': [] });

        }
    }
}

exports.updateProfile = function (req, res, next) {
    user.find({ _id: mongoose.Types.ObjectId(req.body.id) }).exec((err, data) => {

        if (err) {
            res.status(200).json({ 'status': 'error', 'msg': 'something went wrong', 'data': [] });
        }
        else {
            try {
                if (data.length > 0) {
                    var userVariable = {
                        "username": req.body.username,
                        "email": req.body.email,
                        "phoneNumber": req.body.phoneNumber,
                        "country_code": req.body.country_code,
                        "address": req.body.address,
                        "country": mongoose.Types.ObjectId(req.body.country),
                        "state": mongoose.Types.ObjectId(req.body.state),
                        "city": mongoose.Types.ObjectId(req.body.city),
                    }
                    if (data[0].role == 3) {
                        var teacherData = {
                            "subject": req.body.specialism,
                            "school_type": mongoose.Types.ObjectId(req.body.school_type),
                            "skills": req.body.skills,
                            "experience": mongoose.Types.ObjectId(req.body.experience),
                            "education_detail": req.body.educationDetails
                        }
                        if (req.files != undefined) {
                            if (req.files.file != undefined) {
                                teacherData.resume = req.files.file[0].filename;
                            }
                            if (req.files.profile != undefined) {
                                userVariable.profileImage = req.files.profile[0].filename;
                            }
                        }
                    }
                    if (req.files != undefined) {

                        if (req.files.profile != undefined) {     // `${url.base_url}/${req.files.profile[0].filename}`;
                            userVariable.profileImage = req.files.profile[0].filename; //req.files.profile[0].filename

                        }
                        if (req.files.file != undefined) {
                            userVariable.resume = req.files.file[0].filename

                        }
                    }

                    user.update({ _id: mongoose.Types.ObjectId(req.body.id) }, userVariable, function (err, data) {
                        if (err) {
                            console.log(err)
                            res.status(200).json({ 'status': 'error', 'msg': 'something went wrong', 'data': [] });
                        }
                        else {
                            teacher.updateOne({ teacher_id: mongoose.Types.ObjectId(req.body.id) }, teacherData, { new: true }, function (err, updatedTeacher) {
                                if (err) {
                                    res.status(200).json({ 'status': 'error', 'msg': 'something went wrong', 'data': [] });

                                }
                                else {
                                    res.status(200).json({ 'status': 'success', 'msg': 'updated successfuly', 'data': updatedTeacher });
                                }

                            })
                        }
                    })
                }
                else {
                    res.status(200).json({ 'status': 'error', 'msg': 'user not exist', 'data': [] });

                }
            }
            catch (err) {
                res.status(500).json({ 'status': 'error', 'msg': 'something went wrong', 'data': [] });

            }
        }

    })

}

exports.schoolList = function (req, res, next) {
    user.find({ role: 2 }).sort({ createdAt: -1 }).exec(function (err, data) {
        if (err) {
            console.log(err);
            res.status(200).json({ 'status': 'error', 'msg': 'something went wrong', 'data': [] });
        }
        else {
            res.status(200).json({ 'status': 'success', 'msg': 'school list', 'data': data });

        }
    })
}

exports.jobPost = (req, res) => {
    try {

        var jobVariable = {
            schoolName: mongoose.Types.ObjectId(req.body.schoolName),
            location: req.body.location,
            salary: req.body.salary,
            jobType: mongoose.Types.ObjectId(req.body.jobType),
            totalVacancy: req.body.totalVacancy,
            jobDescription: req.body.jobDescription,
            experience: mongoose.Types.ObjectId(req.body.experience),
            professionName: req.body.profession,
            address_lat_lng: req.body.lat_lng,
            skills: req.body.skills,
            // skills:mongoose.Types.ObjectId(req.body.skills),
        }

        if (req.file != undefined) {
            jobVariable.schoolImage = req.file.filename;

        }

        new jobModel(jobVariable).save((saveErr, saveRes) => {
            if (saveErr) {
                console.log("saveErr", saveErr)
                res.status(200).json({ 'status': 'success', 'msg': 'something went wrong ', 'data': [] });
            }
            else {
                res.status(200).json({ 'status': 'success', 'msg': "Job Post Successfully", 'data': saveRes });
            }
        })
    }
    catch (error) {
        console.log(error, 'error')
        res.status(200).json({ 'status': 'success', 'msg': 'something went wrong ', 'data': [] });
    }
}

exports.getPostJobs = function (req, res, next) {

    jobModel.find({ _id: mongoose.Types.ObjectId(req.body.id) }).populate('schoolName', 'username').populate('experience', 'experience').populate('skills', 'subject').exec(function (err, data) {
        if (err) {
            console.log(err)
            res.status(200).json({ 'status': 'error', 'msg': 'something went wrong', 'data': [] });
        }
        else {
            if (data.length > 0) {
                if (data[0].schoolImage != undefined) {
                    data[0].schoolImage = `${url.base_url}/${data[0].schoolImage}`
                }
            }
            res.status(200).json({ 'status': 'success', 'msg': 'Get job post detail', 'data': data });
        }
    })
}

exports.customerSupport = (req, res) => {
    try {
        let userVariable = {
            title: req.body.title,
            description: req.body.description,
            user_id: mongoose.Types.ObjectId(req.body.userId)
        }

        new supportModel(userVariable).save((saveErr, saveRes) => {
            if (saveErr) {
                res.status(200).json({ 'status': 'success', 'msg': 'Something went wrong ', 'data': saveErr });
            }
            else {
                res.status(200).json({ 'status': 'success', 'msg': "customer support successfully done", 'data': saveRes });
            }
        })
    } catch (error) {
        return res.status(500).json({ message: error.message });
    }
}

exports.custSupportList = function (req, res, next) {
    supportModel.find({}).populate("user_id", 'username').sort({ 'createdAt': -1 }).exec(function (err, data) {
        if (err) {
            console.log("Line no 455", err);
            res.status(200).json({ 'status': 'success', 'msg': 'something went wrong ', 'data': [] });

        }
        else {
            res.status(200).json({ 'status': 'success', 'msg': 'Customer support List fetched successfully', 'data': data });

        }
    })
}

exports.postList = async (req, res, next) => {
    var school_id = req.body.school_id;
    if (school_id) {
        try {
            const postList = await jobModel.aggregate([
                {
                    "$match": {
                        "schoolName": mongoose.Types.ObjectId(req.body.school_id)
                    }
                },
                {
                    "$project": {
                        "job": "$$ROOT"
                    }
                },
                {
                    "$sort": {
                        "createdAt": -1
                    }
                },
                {
                    $lookup: {
                        from: "experiences",
                        localField: "job.experience",
                        foreignField: "_id",
                        as: "experience"
                    }
                },
                {
                    "$unwind": {
                        "path": "$experience",
                        "preserveNullAndEmptyArrays": true
                    }
                },
                {
                    "$project": {
                        "schoolName": "$job.schoolName",
                        "experience": "$job.experience",
                        "location": "$job.location",
                        "profession": "$job.professionName",
                        "created": "$job.createdAt",
                        "experience": "$experience.experience",
                    }
                },
            ]);
            res.status(200).json({ 'status': 'success', 'msg': 'Job Post List', 'data': postList })

        } catch (error) {
            res.status(500).json({ 'status': 'error', 'msg': 'something went wrong', 'data': [] })
        }
    }
    else {
        if (req.body.location && req.body.subject && req.body.experience) {
            var queryString = { address_lat_lng: req.body.location, skills: mongoose.Types.ObjectId(req.body.subject), experience: mongoose.Types.ObjectId(req.body.experience) }
        }
        else {
            if (req.body.location && req.body.subject) {
                var queryString = { address_lat_lng: req.body.location, skills: mongoose.Types.ObjectId(req.body.subject) }
            }
            else if (req.body.subject && req.body.experience) {
                var queryString = { skills: mongoose.Types.ObjectId(req.body.subject), experience: mongoose.Types.ObjectId(req.body.experience) }

            }
            else if (req.body.location && req.body.experience) {
                var queryString = { address_lat_lng: req.body.location, experience: mongoose.Types.ObjectId(req.body.experience) }

            }
            else if (req.body.location || req.body.subject || req.body.experience) {
                if (req.body.location) {
                    var queryString = { address_lat_lng: req.body.location }

                }
                if (req.body.subject) {
                    var queryString = { skills: mongoose.Types.ObjectId(req.body.subject) }

                }
                if (req.body.experience) {
                    var queryString = { experience: mongoose.Types.ObjectId(req.body.experience) }

                }
            }
            else {
                var queryString = {}
            }
        }
        try {
            const postList = await jobModel.aggregate([

                {
                    "$match": queryString
                },

                {
                    "$project": {
                        "job": "$$ROOT"
                    }
                },
                {
                    "$sort": {
                        "createdAt": -1
                    }
                },
                {
                    $lookup: {
                        from: "users",
                        localField: "job.schoolName",
                        foreignField: "_id",
                        as: "school"
                    }
                },
                {
                    "$unwind": {
                        "path": "$school",
                        "preserveNullAndEmptyArrays": true
                    }
                },
                {
                    $lookup: {
                        from: "subjects",
                        localField: "job.skills",
                        foreignField: "_id",
                        as: "sub"
                    }
                },
                {
                    "$unwind": {
                        "path": "$sub",
                        "preserveNullAndEmptyArrays": true
                    }
                },
                {
                    $lookup: {
                        from: "experiences",
                        localField: "job.experience",
                        foreignField: "_id",
                        as: "experience"
                    }
                },
                {
                    "$unwind": {
                        "path": "$experience",
                        "preserveNullAndEmptyArrays": true
                    }
                },
                {
                    $lookup: {
                        from: "school_types",
                        localField: "job.jobType",
                        foreignField: "_id",
                        as: "schoolType"
                    }
                },
                {
                    "$unwind": {
                        "path": "$schoolType",
                        "preserveNullAndEmptyArrays": true
                    }
                },

                {
                    "$project": {
                        "school_id": "$job.schoolName",
                        "school_id": "$job.schoolName",
                        "schoolName": "$school.username",
                        "schoolNumber": "$school.schoolNumber",
                        "profileImage": "$school.profileImage",
                        "experience_id": "$job.experience",
                        "location": "$job.location",
                        "profession": "$job.professionName",
                        "created": "$job.createdAt",
                        'subject': "$job.skills",
                        "lat_long": "$job.address_lat_lng",
                        "experience": "$experience.experience"
                    }
                }
            ]);
            postList1 = postList.map(async obj => {
                if (obj.profileImage) {
                    obj.profileImage = `${url.base_url}/${obj.profileImage}`;
                }
                else {
                    obj.profileImage = '';
                }
                return obj;
            })
            let PostList = await Promise.all(postList1);

            let JsonReversedArray = PostList.reverse();

            res.status(200).json({ 'status': 'success', 'msg': 'Job Post List', 'data': JsonReversedArray })

        } catch (error) {
            res.status(500).json({ 'status': 'error', 'msg': 'something went wrong', 'data': [] })
        }
    }
}

exports.editPostJobs = function (req, res, next) {
    jobModel.findOne({ _id: (req.body._id) }).exec(function (err, data) {
        if (err) {
            console.log(err)
            res.status(200).json({ 'status': 'error', 'msg': 'something went wrong', 'data': [] });
        }
        else {
            const body = req.body;

            const schoolName = body.schoolName;
            const location = body.location;
            const salary = body.salary;
            const jobType = body.jobType;
            const totalVacancy = body.totalVacancy;
            const jobDescription = body.jobDescription;
            const experience = body.experience;
            const updates = {
                schoolName,
                location,
                salary,
                jobType,
                totalVacancy,
                jobDescription,
                experience
            };
            if (req.file) {
                const schoolImage = req.file.filename;
                updates.schoolImage = schoolImage;
            }

            jobModel.findByIdAndUpdate({ _id: data._id }, { $set: updates }, { new: true }, (updateErr, updateRes) => {
                if (updateErr) {
                    res.status(200).json({ 'status': 'error', 'msg': 'Internal server error', 'data': [] });
                } else {
                    res.status(200).json({ 'status': 'success', 'msg': 'edit job post detail', 'data': updateRes });
                }
            })
        }
    })
}


exports.candidateList = async function (req, res, next) {

    var postList = await jobModel.aggregate([
        {
            "$match": {
                "schoolName": mongoose.Types.ObjectId(req.body.school_id)
            }
        },

        {

            "$project": {
                "job": "$$ROOT"
            }
        },
        {
            "$sort": {
                "createdAt": -1
            }
        },
        {
            "$project": {
                "job_id": "$job._id",

            }
        }
    ]);
    let postListJson = postList.reverse();

    postList1 = postListJson.reduce((acc, obj) => {
        acc.push(obj.job_id);
        return acc
    }, [])

    var appliedForJob = await applied_for_job.aggregate([
        {
            "$match":
                { "job_id": { "$in": postList1 } }
        },

        {
            "$project": {
                "ca": "$$ROOT"
            }
        },
        {
            "$sort": {
                "createdAt": -1
            }
        },
        {
            $lookup: {
                from: "users",
                localField: "ca.user_id",
                foreignField: "_id",
                as: "user"
            }
        },
        {
            "$unwind": {
                "path": "$user",
                "preserveNullAndEmptyArrays": true
            }
        },
        {
            $lookup: {
                from: "jobs",
                localField: "ca.job_id",
                foreignField: "_id",
                as: "job"
            }
        },
        {
            "$unwind": {
                "path": "$job",
                "preserveNullAndEmptyArrays": true
            }
        },
        {
            $lookup: {
                from: "teachers",
                localField: "ca.user_id",
                foreignField: "teacher_id",
                as: "teacher"
            }
        },
        {
            "$unwind": {
                "path": "$teacher",
                "preserveNullAndEmptyArrays": false
            }
        },
        {
            "$project": {
                "user_id": "$teacher.teacher_id",
                "job_id": "$job._id",
                "job_title": "$job.professionName",
                "candidate_name": "$user.username",
                "candidate_email": "$user.email",
                "candidate_address": "$user.address",
                "subject": "$teacher.subject"


            }
        }
    ])

    let listOfCandidate = await Promise.all(appliedForJob);
    let JsonReversedArray = listOfCandidate.reverse();
    res.send(JsonReversedArray);
}

exports.jobPostApplied = async (req, res) => {
    var jobData = await jobs.find({ _id: mongoose.Types.ObjectId(req.body.job_id) }).exec();
    if (jobData.length) {
        try {
            jobAppliedModel.find({ $and: [{ user_id: mongoose.Types.ObjectId(req.body.user_id) }, { job_id: mongoose.Types.ObjectId(req.body.job_id) }] }, (err, data) => {
                if (err) {
                    res.status(200).json({ 'status': 'error', 'msg': 'something went wrong ', 'data': [] });

                }
                else if (data.length > 0) {
                    res.status(200).json({ 'status': 'success', 'msg': "you already applied for this job", 'data': [] });
                }
                else {
                    let userJobId = {
                        user_id: mongoose.Types.ObjectId(req.body.user_id),
                        job_id: mongoose.Types.ObjectId(req.body.job_id),
                        status: 1
                    }

                    new jobAppliedModel(userJobId).save(async (saveErr, saveRes) => {
                        if (saveErr) {
                            res.status(200).json({ 'status': 'success', 'msg': 'Something went wrong ', 'data': saveErr });
                        }
                        else {

                            var userVariable = {
                                job_id: saveRes.job_id,
                                school_id: saveRes.schoolName,
                                applicant_id: saveRes.user_id,
                                job_status: 1,
                                application_id: saveRes._id,

                            }

                            new job_statuses(userVariable).save().then(data => {

                                res.status(200).json({ 'status': 'success', 'msg': "Job Post Successfully", 'data': saveRes });
                            }).catch(err => {
                                res.status(200).json({ 'status': 'success', 'msg': "something went wrong", 'data': [] });

                            })
                            var jobData = await jobModel.find({ _id: mongoose.Types.ObjectId(req.body.job_id) }).exec();
                            console.log(jobData[0].professionName);

                            var loginUser = await login_logModel.find({ user_id: mongoose.Types.ObjectId(jobData[0].schoolName) }).exec();
                            var data = {
                                title: 'Job Applied',
                                message: `Teacher has been successfully applied for the job(${jobData[0].professionName})`,
                                device_type: 'web',
                                userId: jobData[0].schoolName,
                                device_token: loginUser[0].device_token
                            }
                            console.log(data, "fcm data line no 1722")
                            const result = await send_fcm(data);
                            console.log(result, "Result Fcm")
                        }
                    })
                }
            })
        }
        catch (error) {
            res.status(500).json({ 'status': 'error', 'msg': "something went wrong", 'data': [] });
        }
    }
    else {
        res.status(200).json({ 'status': 'error', 'msg': "Invalid Job", 'data': [] });

    }
}

exports.addFeedback = function (req, res, next) {
    var userVariable = {
        feedback: req.body.feedback,
        user_id: mongoose.Types.ObjectId(req.body.user_id),
        school_id: mongoose.Types.ObjectId(req.body.school_id),
        rating: req.body.rating
    }
    new feedback(userVariable).save().then(data => {
        res.status(200).json({ 'status': 'success', 'msg': "feedback added successfuly", 'data': data });
    }).catch((error) => {
        console.log(error)
        res.status(500).json({ 'status': 'error', 'msg': "something went wrong", 'data': [] });
    });
}

exports.addQuery = async function (req, res, next) {
    var userVariable = {
        query: req.body.query,
        user_id: mongoose.Types.ObjectId(req.body.user_id)
    }

    new query(userVariable).save().then(data => {
        res.status(200).json({ 'status': 'success', 'msg': 'added query for support ', 'data': data });
    }).catch((error) => {
        console.log(error)
        res.status(200).json({ 'status': 'error', 'msg': 'something went wrong', 'data': [] });
    });
}

exports.view_job_applied_by_user = async function (req, res, next) {
    try {
        var dtaFromaggregate = await jobAppliedModel.aggregate([
            {
                "$match":

                {
                    "$and": [
                        {
                            job_id: mongoose.Types.ObjectId(req.body.job_id),
                            user_id: mongoose.Types.ObjectId(req.body.user_id)
                        }
                    ]
                },
            },
            {
                "$project": {
                    "ca": "$$ROOT"
                }
            },
            {
                "$lookup": {
                    from: "jobs",
                    localField: "ca.job_id",
                    foreignField: "_id",
                    as: "job"
                }
            },
            {
                "$unwind": {
                    "path": "$job",
                    "preserveNullAndEmptyArrays": true
                }
            },
            {
                "$lookup": {
                    from: "teachers",
                    localField: "ca.user_id",
                    foreignField: "teacher_id",
                    as: "teacher"
                }
            },
            {
                "$unwind": {
                    "path": "$teacher",
                    "preserveNullAndEmptyArrays": true
                }
            },
            {
                "$lookup": {
                    from: "users",
                    localField: "ca.user_id",
                    foreignField: "_id",
                    as: "teacher_user"
                }
            },
            {
                "$unwind": {
                    "path": "$teacher_user",
                    "preserveNullAndEmptyArrays": true
                }
            },
            {
                $lookup: {
                    from: "countries",
                    localField: "teacher_user.country",
                    foreignField: "_id",
                    as: "co"
                }
            },
            {
                "$unwind": {
                    "path": "$co",
                    "preserveNullAndEmptyArrays": false
                }
            },
            {
                $lookup: {
                    from: "states",
                    localField: "teacher_user.state",
                    foreignField: "_id",
                    as: "st"
                }
            },
            {
                "$unwind": {
                    "path": "$st",
                    "preserveNullAndEmptyArrays": false
                }
            },
            {
                $lookup: {
                    from: "cities",
                    localField: "teacher_user.city",
                    foreignField: "_id",
                    as: "ct"
                }
            },
            {
                "$unwind": {
                    "path": "$ct",
                    "preserveNullAndEmptyArrays": false
                }
            },
            {
                $project: {
                    "teacher_username": "$teacher_user.username",
                    "teacher_experience": "$teacher.experience",
                    "teacher_skills": "$teacher.subject",
                    "teacher_address": "$teacher_user.address",
                    "teacher_resume": "$teacher.resume",
                    "teacher_profile": "$teacher_user.profileImage",
                    "teacher_email": "$teacher_user.email",
                    "teacher_number": "$teacher_user.phoneNumber",
                    "teacher_country_code": "$teacher_user.country_code",
                    "job_title": "$job.professionName",
                    "jobType": "$job.jobType",
                    "job_totalVacancy": "$job.totalVacancy",
                    "job_experience": "$job.experience",
                    "job_subject": "$job.skills",
                    "schoolName": "$job.schoolName",
                    "status": "$ca.status",
                    "job_id": "$job._id",
                    "teacher_id": "$teacher_user._id",
                    "application_id": "$ca._id",
                    "country_name": "$co.name",
                    "state_name": "$st.name",
                    "city_name": "$ct.name",
                }
            }
        ])
        // console.log(dtaFromaggregate, "view job applied by user Detail")
        var jobType_list = await schoolType.find({ _id: mongoose.Types.ObjectId(dtaFromaggregate[0].jobType) }).select('type').exec();
        // var experience_list = await experiences.find({ _id: { $in: [dtaFromaggregate[0].teacher_experience, dtaFromaggregate[0].job_experience] } }).select('experience').exec();
        // var subject_list = await subject.find({ _id: { $in: [dtaFromaggregate[0].teacher_skills, dtaFromaggregate[0].job_subject] } }).exec()
        var jobExperience_list = await experiences.find({ _id: dtaFromaggregate[0].job_experience }).select('experience').exec();
        var teacherExperience_list = await experiences.find({ _id: dtaFromaggregate[0].teacher_experience }).select('experience').exec();

        dtaFromaggregate = dtaFromaggregate.map(obj => {
            let teacherExperience = teacherExperience_list.filter(data => String(data._id) === String(obj.teacher_experience));
            if (teacherExperience.length > 0) {
                obj.teacherExperience = teacherExperience[0].experience;
            }
            else {
                obj.teacherExperience = null
            }

            let jobExperience = jobExperience_list.filter(data => String(data._id) === String(obj.job_experience));

            if (jobExperience.length > 0) {
                obj.jobExperience = jobExperience[0].experience;
            }
            else {
                obj.jobExperience = null
            }


            let jobType1 = jobType_list.filter(data => String(data._id) === String(obj.jobType));
            if (jobType1.length > 0) {
                obj.jobType = jobType1[0].type;
            }
            else {
                obj.jobType = '';
            }

            //subject

            // let teacherskill = subject_list.filter(data => String(data._id) === String(obj.teacher_skills));
            // obj.teacherskill = teacherskill[0].subject;
            // let jobSkill = subject_list.filter(data => String(data._id) === String(obj.job_subject));
            // obj.jobSkill = jobSkill[0].subject;

            return obj;
        })


        // jobAppliedModel.find({"$and":[{job_id:mongoose.Types.ObjectId(req.body.job_id)},{user_id:mongoose.Types.ObjectId(req.body.user_id)}]}).populate('user_id').populate('job_id').exec().then(data=>{
        let school_id = dtaFromaggregate[0].schoolName;
        //  console.log(school_id, "school_id'")
        if (dtaFromaggregate[0].status == 0) {
            jobAppliedModel.updateOne({ job_id: mongoose.Types.ObjectId(req.body.job_id) }, { user_id: mongoose.Types.ObjectId(req.body.user_id) }, { $set: { status: 1 } }).then(doc => {
                var userVariable = {
                    job_id: dtaFromaggregate[0].job_id,
                    school_id: school_id,
                    applicant_id: dtaFromaggregate[0].teacher_id,
                    job_status: 2,
                    application_id: data[0].application_id
                }
                new job_statuses(userVariable).save().then(status => {
                    res.status(200).json({ 'status': 'success', 'msg': 'job applied by user ', 'data': dtaFromaggregate });
                }).catch(err => {
                    console.log(err, 'error')
                    res.status(200).json({ 'status': 'error', 'msg': 'order status not updated', 'data': dtaFromaggregate });
                })
            }).catch(err => {
                console.log(err, 'error')

                res.status(200).json({ 'status': 'error', 'msg': 'order status not updated', 'data': [] });

            })
        }
        else {
            res.status(200).json({ 'status': 'success', 'msg': 'job applied by user  ', 'data': dtaFromaggregate });

        }
    }
    catch (err) {
        console.log(err);
        res.status(500).json({ 'status': 'error', 'msg': 'something went wrong', 'data': [] });

    }

}

exports.update_applied_job_status = async function (req, res, next) {
    var userVariable = {
        job_id: req.body.job_id,
        school_id: req.body.school_id,
        applicant_id: req.body.applicant_id,
        job_status: req.body.job_status,
        application_id: req.body.application_id
    }

    var notificationVariable = {
        user_id: req.body.applicant_id,
        application_id: req.body.application_id,
        job_id: req.body.job_id,
        status: req.body.job_status
    }
    try {
        jobAppliedModel.updateOne({ _id: mongoose.Types.ObjectId(req.body.application_id) }, { $set: { status: req.body.job_status } }).then(async doc => {
            console.log(doc.status, "jobModel job status")
            await new notificationList(notificationVariable).save();
            new job_statuses(userVariable).save().then(async status => {
                console.log(status.job_status, "status");


                const schoolData = await user.find({ _id: mongoose.Types.ObjectId(req.body.school_id) }).exec();
                 var loginUser = await login_logModel.find({ user_id: mongoose.Types.ObjectId(req.body.applicant_id) }).exec();

                if (status.job_status = 4) {
                    console.log(status.job_status, "Yess line no 1661");
                    var data = {
                        title: 'Your job application is accepted',
                        message: `Your resume for job application is accepted by ${schoolData[0].username}`,
                        device_type: 'web',
                        userId: req.body.applicant_id,
                        device_token: loginUser[0].device_token
                        // device_token: loginUser[0].device_token

                    }
                    console.log(data, "fcm data line no 1722")
                    const result = await send_fcm(data);
                    console.log(result, "Result Fcm")
                    res.status(200).json({ 'status': 'success', 'msg': 'job status updated succesfully', 'data': status });
                }
                //     else if (status.job_status = 5) {
                //     var data = {
                //         title: 'Your Resume is shortlisted',
                //         message: 'Your resume for job application is selected & send mail on your registered email-id by school',
                //         device_type: 'web',
                //         userId: req.body.applicant_id,

                //       //  device_token: loginUser[0].device_token
                //        // device_token: loginUser[0].device_token

                //     }
                //     console.log(data, "fcm data line no 1735")
                //     const result = await send_fcm(data);
                //     console.log(result, "Result Fcm")
                //     res.status(200).json({ 'status': 'success', 'msg': 'job status updated succesfully', 'data': status });
                // }
                else {
                    res.status(200).json({ 'status': 'success', 'msg': 'job status updated succesfully', 'data': status });
                }
            }).catch(err => {
                res.status(200).json({ 'status': 'error', 'msg': 'something went wrong', 'data': err });
            })
        }).catch(err => {
            res.status(200).json({ 'status': 'error', 'msg': 'something went wrong', 'data': err });
        })
    }
    catch (err) {
        console.log('err', err);
        res.status(500).json({ 'status': 'error', 'msg': 'something went wrong', 'data': [] })
    }
}


exports.view_job_status = async function (req, res, next) {
    job_statuses.find({ application_id: req.body.id }).exec().then(data => {
        res.status(200).json({ 'status': 'success', 'msg': 'job status updated succesfully', 'data': data });
    }).catch(err => {
        res.status(200).json({ 'status': 'success', 'msg': 'something went wrong', 'data': err });

    })
}

exports.job_detail = async function (req, res, next) {
    try {                                                                    // .populate('skills', 'subject')
        jobPost.find({ _id: mongoose.Types.ObjectId(req.body.job_id) }).populate('schoolName').populate('experience', 'experience').populate('jobType', 'type').exec().then(data => {
            data1 = data.map(obj => {
                ;
                if (obj.schoolName.profileImage) {

                    obj.schoolName.profileImage = `${url.base_url}/${obj.schoolName.profileImage}`;
                }
                else {
                    obj.schoolName.profileImage = '';
                }
                return obj;
            })
            jobAppliedModel.find({ user_id: mongoose.Types.ObjectId(req.body.user_id), job_id: mongoose.Types.ObjectId(req.body.job_id) }).select('status').exec().then(doc => {

                if (doc.length > 0) var newJson = { "job_detail": data1[0], job_status: doc[0].status };
                else var newJson = { "job_detail": data[0], job_status: 0 };

                res.status(200).json({ 'status': 'success', 'msg': 'job detail', 'data': newJson });
            }).catch(err => {
                console.log(err)
                res.status(200).json({ 'status': 'success', 'msg': 'something went wrong', 'data': [] });

            })
        }).catch(err => {
            console.log(err)
            res.status(200).json({ 'status': 'error', 'msg': 'something went wrong', 'data': [] });

        })
    }
    catch (err) {
        console.log(err);
        res.status(200).json({ 'status': 'error', 'msg': 'something went wrong', 'data': [] });
    }

}
function sendMessage(userEmail, title, message,) {
    var text = `${title}
    ${message}`
    var transporter = nodemailer.createTransport({
        service: 'gmail',
        auth: {
            user: 'maxtratechnologies.test@gmail.com',
            pass: 'Maxtra@123'
        }
    });

    var mailOptions = {
        from: 'OTRA maxtratechnologies.test@gmail.com',
        to: userEmail,
        subject: 'otp verification',
        text: text
    };

    transporter.sendMail(mailOptions, function (error, info) {
        if (error) {
            console.log(error, null);
        } else {
            console.log(null, info);
        }
    })
}

exports.sendMessage = async function (req, res, next) {
    var title = req.body.title;
    let titleBold = title.bold();
    var message = req.body.text;
    var school_id = req.body.school_id;
    var user_id = req.body.user_id;
    var application_id = req.body.application_id
    var userVariable = { message, school_id, user_id, title, application_id }
    try {
        const schoolData = await user.find({ _id: mongoose.Types.ObjectId(school_id) });
        const schoolEmail = schoolData[0].email;
        // console.log(schoolEmail, "cmnghtlbonlb");

        const userData = await user.find({ _id: mongoose.Types.ObjectId(user_id) });
        const userEmail = userData[0].email;
        //  console.log(userEmail, "cmnghtlbonlb");

        new invitationMessage(userVariable).save().then(async data => {
            //  console.log(data);
            sendMessage(userEmail, titleBold, message, (emailErr, emailRes) => {

                if (emailErr) {
                    return res.send({ responseCode: 500, responseMessage: "Internal server error.", responseResult: emailErr });
                } else {

                }
            })

             var loginUser = await login_logModel.find({ user_id: mongoose.Types.ObjectId(req.body.user_id) }).exec();
            console.log(loginUser[0].device_token);
             var data = {
                title: 'Your Resume is shortlisted',
                message: `Your resume for job application is selected & send mail on your registered email-id by ${schoolData[0].username}`,
                device_type: 'ios',
                userId: req.body.user_id,

                device_token: loginUser[0].device_token
                // device_token: loginUser[0].device_token

            }

            console.log(data, "fcm data line no 1857");
            const result = await send_fcm(data);

            console.log(data, "fcm data line no 1735")
           


            res.status(200).json({ 'status': 'success', 'msg': 'message sent successfully', 'data': data });

        }).catch(err => {
            res.status(200).json({ 'status': 'success', 'msg': 'something went wrong', 'data': [] });
        })
    }
    catch (err) {
        console.log(err)
        res.status(200).json({ 'status': 'error', 'msg': 'something went wrong', 'data': [] });
    }
}

exports.invitationMessageList = async function (req, res, next) {

    invitationMessage.find({ user_id: mongoose.Types.ObjectId(req.body.user_id) }).populate('user_id', 'username').populate('school_id', 'username').select(["-message"]).exec().then(data => {
        data = data.map(obj => {
            obj = {
                user_id: obj.user_id._id,
                school_name: obj.user_id.username,
                school_id: obj.school_id._id,
                username: obj.school_id.username,
                title: obj.title,
                id: obj._id
            }
            return obj;
        })

        res.status(200).json({ 'status': 'success', 'msg': 'messages list data get successfully', 'data': data });
    }).catch(err => {
        res.status(500).json({ 'status': 'error', 'msg': 'something went wrong', 'data': [] });
    })
}

exports.invitationDetail = async function (req, res, next) {
    invitationMessage.find({ _id: mongoose.Types.ObjectId(req.body.message_id) }).populate('user_id', 'username').populate('school_id', 'username profileImage').exec().then(data => {
        data[0].school_id.profileImage = `${url.base_url}/${data[0].school_id.profileImage}`
        res.status(200).json({ 'status': 'success', 'msg': 'message detail get successfully', 'data': data });

    }).catch(err => {
        console.log(err, 'error')
        res.status(200).json({ 'status': 'error', 'msg': 'something went wrong', 'data': [] })
    })
}

exports.planList = async function (req, res, next) {
    subscription.find({}).exec().then(data => {
        res.status(200).json({ 'status': 'success', 'msg': 'plan list data', 'data': data });

    }).catch(err => {
        console.log(err, 'error in planList app api')
        res.status(200).json({ 'status': 'error', 'msg': 'something went wrong', 'data': [] })
    })
}


exports.openToWork_status = async function (req, res, next) {
    var openToWork = req.body.openToWork
    teacher.updateOne({ teacher_id: req.body.id }, { $set: { openToWork } }).then(data => {
        res.status(200).json({ 'status': 'success', 'msg': 'status updated successfuly', 'data': [] });
    }).catch(err => {
        console.log(err, 'eror openToWork')
        res.status(200).json({ 'status': 'error', 'msg': 'something went wrong', 'data': [] });

    })
}


exports.teacher_list_searching_for_job = async function (req, res, next) {
    teacher.find({ openToWork: true }).exec().then(data => {
        if (data.length > 0) {
            res.status(200).json({ 'status': 'success', 'msg': 'teachers list looking for job change', 'data': data });
        }
        else {
            res.status(200).json({ 'status': 'error', 'msg': 'No data found', 'data': [] });

        }
    }).catch(err => {
        res.status(200).json({ 'status': 'success', 'msg': 'something went wrong', 'data': [] });

    })
}

exports.customerSupport_app = async function (req, res, next) {
    user.find({ role: 1 }, { email: 1, phoneNumber: 1 }).select().exec().then(data => {
        res.status(200).json({ 'status': 'success', 'msg': 'admin details get successfully', 'data': data });
    }).catch(err => {
        res.status(500).json({ 'status': 'success', 'msg': 'something went wrong', 'data': [] });

    })
}

exports.query_list_user = function (req, res, next) {
    query.find({ user_id: mongoose.Types.ObjectId(req.body.user_id) }).exec().then(data => {


        let queryList = data.reverse();

        res.status(200).json({ 'status': 'success', 'msg': 'query list', 'data': queryList });
    }).catch(err => {
        console.log(err, 'errr===')
        res.status(200).json({ 'status': 'error', 'msg': 'something went wrong ', 'data': data });

    })
}

exports.view_mail = function (req, res, next) {
    invitationMessage.find({ application_id: mongoose.Types.ObjectId(req.body.application_id) }).exec().then(data => {
        res.status(200).json({ 'status': 'success', 'msg': 'view mail ', 'data': data });
    }).catch(data => {
        res.status(200).json({ 'status': 'error', 'msg': 'something went wrong ', 'data': [] });

    })
}

exports.notification_list = function (req, res, next) {
    try {
        notifications.find({ user_id: mongoose.Types.ObjectId(req.body.user_id) }).lean().
            exec().then(data => {
                data = data.map(obj => {
                    if (obj.status) {
                        if (obj.status == 2) {
                            obj.status = 'Job application viewed by school'
                        }
                        else if (obj.status == 3) {
                            obj.status = 'Job application rejected by school'
                        }
                        else if (obj.status == 4) {

                            obj.status = 'Job application accepted by school'

                        }
                        else if (obj.status == 5) {
                            obj.status = 'sent mail on your registered email id by school'
                        } else if (obj.status == 6) {
                            obj.status = 'New teacher apply for job'
                        }
                    }
                    return obj

                })
                res.status(200).json({ 'status': 'success', 'msg': 'notifications list ', 'data': data });

            }).catch((error) => {
                console.log(error, 'error')
                res.status(200).json({ 'status': 'error', 'msg': 'something went wrong ', 'data': [] });

            })
    }
    catch (err) {
        console.log(err, 'error')
        res.status(200).json({ 'status': 'error', 'msg': 'something went wrong ', 'data': [] });
    }
}


exports.jobApplied_listing = async function (req, res, next) {
    try {

        var dtaFromaggregate = await jobAppliedModel.aggregate([
            {
                "$match":
                {
                    user_id: mongoose.Types.ObjectId(req.body.user_id)

                },
            },
            {
                "$project": {
                    "ca": "$$ROOT"
                }
            },
            {
                "$sort": {
                    "createdAt": -1
                }
            },
            {
                "$lookup": {
                    from: "jobs",
                    localField: "ca.job_id",
                    foreignField: "_id",
                    as: "job"
                }
            },
            {
                "$unwind": {
                    "path": "$job",
                    "preserveNullAndEmptyArrays": true
                }
            },

            {
                $project: {
                    "job_experience": "$job.experience",
                    "job_skill": "$job.skills",
                    "schoolName": "$job.schoolName",
                    "schoolNumber": "$job.schoolName",
                    "profileImage": "$job.schoolName",
                    "location": "$job.location",
                    "status": "$ca.status",
                }
            }
        ])
        //  console.log(dtaFromaggregate, "dtaFromaggregate line no 1596")
        dtaFromaggregate = dtaFromaggregate.map(async obj => {

            //  console.log(obj, "==========================================================='")

            let job_experience = await experiences.find({ _id: mongoose.Types.ObjectId(obj.job_experience) }).exec();
            //  console.log(job_experience, "job_experience")
            if (job_experience.length > 0) {
                obj.job_experience = job_experience[0].experience;
                //   console.log(obj.job_experience, "job_experienceghhhhhhhhhhhhhhhhhhhh")
            }

            var schoolName = await user.find({ _id: mongoose.Types.ObjectId(obj.schoolName) }).select('username').exec();
            console.log(schoolName, "schoolName")
            if (schoolName.length > 0) {
                obj.schoolName = schoolName[0].username;
            }
            else {
                obj.schoolName = '';
            }
            var schoolNumber1 = await user.find({ _id: mongoose.Types.ObjectId(obj.schoolNumber) }).select('schoolNumber').exec();;
            // console.log(schoolNumber1, "schoolNumber")
            if (schoolNumber1.length > 0) {
                obj.schoolNumber = schoolNumber1[0].schoolNumber;
            }
            else {
                obj.schoolNumber = '';
            }
            var profileImage = await user.find({ _id: mongoose.Types.ObjectId(obj.profileImage) }).select('profileImage').exec();
            // console.log(profileImage, "profileImage")
            if (profileImage.length > 0) {
                obj.profileImage = `${url.base_url}/${profileImage[0]?.profileImage}`;
            }
            else {
                obj.profileImage = '';
            }
            return obj;
        })

        let view_jobApplied_list = await Promise.all(dtaFromaggregate);

        res.status(200).json({ 'status': 'success', 'msg': 'job Appliied list by user', 'data': view_jobApplied_list });

    }
    catch (err) {
        console.log(err);
        res.status(500).json({ 'status': 'error', 'msg': 'something went wrong', 'data': [] });
    }
}

exports.candidateListEligible = async function (req, res, next) {

    var appliedForJob = await applied_for_job.aggregate([
        {
            "$match":
                { job_id: mongoose.Types.ObjectId(req.body.job_id), }
        },

        {
            "$project": {
                "ca": "$$ROOT"
            }
        },
        {
            "$sort": {
                createdAt: -1
            }
        },
        {
            $lookup: {
                from: "users",
                localField: "ca.user_id",
                foreignField: "_id",
                as: "user"
            }
        },
        {
            "$unwind": {
                "path": "$user",
                "preserveNullAndEmptyArrays": true
            }
        },
        {
            $lookup: {
                from: "jobs",
                localField: "ca.job_id",
                foreignField: "_id",
                as: "job"
            }
        },
        {
            "$unwind": {
                "path": "$job",
                "preserveNullAndEmptyArrays": true
            }
        },
        {
            $lookup: {
                from: "teachers",
                localField: "ca.user_id",
                foreignField: "teacher_id",
                as: "teacher"
            }
        },
        {
            "$unwind": {
                "path": "$teacher",
                "preserveNullAndEmptyArrays": false
            }
        },
        {
            $lookup: {
                from: "countries",
                localField: "user.country",
                foreignField: "_id",
                as: "co"
            }
        },
        {
            "$unwind": {
                "path": "$co",
                "preserveNullAndEmptyArrays": false
            }
        },
        {
            $lookup: {
                from: "states",
                localField: "user.state",
                foreignField: "_id",
                as: "st"
            }
        },
        {
            "$unwind": {
                "path": "$st",
                "preserveNullAndEmptyArrays": false
            }
        },
        {
            $lookup: {
                from: "cities",
                localField: "user.city",
                foreignField: "_id",
                as: "city"
            }
        },
        {
            "$unwind": {
                "path": "$city",
                "preserveNullAndEmptyArrays": false
            }
        },
        {
            "$project": {
                "user_id": "$teacher.teacher_id",
                "job_id": "$job._id",
                "job_title": "$job.professionName",
                "userName": "$user.username",
                "subject": "$teacher.subject",
                "schoolType": "$teacher.school_type",
                "experience": "$teacher.experience",
                "profilePic": "$user.profileImage",
                "email": "$user.email",
                "countryCode": "$user.countryCode",
                "phoneNumber": "$user.phoneNumber",
                "address": "$user.address",
                "resume": "$teacher.resume",
                "status": "$ca.status",
                "country_name": "$co.name",
                "state_name": "$st.name",
                "city_name": "$city.name",

            }
        }
    ])

    appliedForJob = appliedForJob.map(async obj => {

        //  console.log(obj, "==========================================================='")

        let job_experience = await experiences.find({ _id: mongoose.Types.ObjectId(obj.experience) }).exec();

        if (job_experience.length > 0) {
            obj.experience = job_experience[0].experience;

        } else {
            obj.experience = '';
        }

        let school_type = await schoolType.find({ _id: mongoose.Types.ObjectId(obj.schoolType) }).exec();

        if (school_type.length > 0) {
            obj.schoolType = school_type[0].type;
        } else {
            obj.schoolType = '';
        }

        if (obj.profilePic) {
            obj.profilePic = `${url.base_url}/${obj.profilePic}`;
        }
        else {
            obj.profilePic = '';
        }

        if (obj.resume) {
            obj.resume = `${url.base_url}/${obj.resume}`;
        }
        else {
            obj.resume = '';
        }
        return obj;
    })
    let listOfCandidate = await Promise.all(appliedForJob);

    let JsonReversedArray = listOfCandidate.reverse();

    return res.send({ 'status': 'true', msg: "Details have been fetched successfully.", data: JsonReversedArray })

}

exports.test = async function (req, res) {
    data = { device_token: 'cCsxDrPASga8KyUQl2OSBY:APA91bGEp4EOEzlLMCvG90khbbY0WCK5rldQR32TiK5hLtbnjmnbutv7z7kKEzU1_5RETX7qr14kcvjlwTSr09aQdfa2ex4VqyqEJmAENHpMe-OlJpwA3uzcK2eQ_8QPvGBtEfPQmNAR', device_type: 'android', title: 'test', message: 'Test Success' }
    send_fcm(data)
}

// exports.send_fcm = async function (req, res) {
//     var FCM = require('fcm-node')

//     // var serverKey = 'AAAAVI-6sUY:APA91bFwE-iIEmrkfRs_qjsVzTimGkJv9iUs4Oy2EKQ9tZV83TyF4WseGvHVWkOHVzC_FtKHqONvXLo-jKwyOSoDRQuIeynHKt-6C3aE02NpUlEy3Y--4BqoSWl5aMLyGIyX2BZN9sww' //put the generated private key path here    
//     // OTRA Server KEY
//     var serverKey = 'AAAAY52i4Bc:APA91bEx9msnbcCJcCmKFpDZPOhRHpMePqfX1PnYnhSluIKxqjSWDzFmgNjIynhjfXf33m37QU1UexsE89X29LYg9z1CxczxhtqZg5RONymPbFQV_ZjAGwkRduheUrUku6sGvHr-Uy9Y'

//     var fcm = new FCM(serverKey)

//     var sound = "";

//     // console.log(message)

//     var note = new apn.Notification();
//     note.badge = 8;
//     note.sound = sound;
//     note.alert = `\uD83D\uDCE7 \u2709 Test`;
//     note.payload = { 'messageFrom': 'John Appleseed' };
//     note.topic = 'com.maxtratechnologies.CUCInow-User';
//     apnProvider.send(note, '934483377c9fd38418ca67ffbf44e5310939be798a395bc568cfd07eedc302f7').then((result) => {
//         console.log(result, result.failed[0], 'result')
//         return true;
//     });

// }

exports.testFcm = async function (req, res) {

    var data = {
        title: req.body.title,
        message: req.body.message,
        device_type: req.body.device_type
    }
    console.log(data)
    const result = await send_fcm(data);
    res.send({ 'status': 'true', msg: " successfully done", data: result })
    console.log(result, "line no 2292");
}

async function send_fcm(data) {
    var FCM = require('fcm-node');

    // var serverKey = 'AAAAVI-6sUY:APA91bFwE-iIEmrkfRs_qjsVzTimGkJv9iUs4Oy2EKQ9tZV83TyF4WseGvHVWkOHVzC_FtKHqONvXLo-jKwyOSoDRQuIeynHKt-6C3aE02NpUlEy3Y--4BqoSWl5aMLyGIyX2BZN9sww' //put the generated private key path here    
    // var serverKey = 'AAAAmDVygCs:APA91bEIkrdmetOLLAZ0vROtTcgzTtsGJuAhQnKRzyIpp2vd7vg9juxzScrxu4vPX1qErzVrf-0S8R-5m8zpo47IdmdQMc_U2BtHRN31d5HIV_pA1Hv2YMwC5ce9jr7AYe7drBwpViN9' //put the generated private key path here    

    // var serverKey = 'AAAAZJZyAPs:APA91bEn2kOHNri1TsAC5As2OUrzZjkM3pHzeSJpRN1YR3UbCCR7DpCzJARuvXPT3-1bjQhnzEXsmX-DsGrJKgSjnUXNaOcPA4WkOA2k2X7ioXASOfpFRPbNl4CbsusFH4yD0Wpxt_zL';
    // OTRA Server Key
    var serverKey = 'AAAAY52i4Bc:APA91bEx9msnbcCJcCmKFpDZPOhRHpMePqfX1PnYnhSluIKxqjSWDzFmgNjIynhjfXf33m37QU1UexsE89X29LYg9z1CxczxhtqZg5RONymPbFQV_ZjAGwkRduheUrUku6sGvHr-Uy9Y'
    var fcm = new FCM(serverKey);

    var sound = "";
    if (data.device_type == "ios" || data.device_type == 'web') {
        var message = { //this may vary according to the message type (single recipient, multicast, topic, et cetera)
            // to: "cCsxDrPASga8KyUQl2OSBY:APA91bGEp4EOEzlLMCvG90khbbY0WCK5rldQR32TiK5hLtbnjmnbutv7z7kKEzU1_5RETX7qr14kcvjlwTSr09aQdfa2ex4VqyqEJmAENHpMe-OlJpwA3uzcK2eQ_8QPvGBtEfPQmNAR",
            // to: "c2Pn2f1mvwg:APA91bHL8S0GPuW6xpeehUBo3zTE1XERnXFk84w44SjCit-i2osO5_yYQokoPGAr1tPmMgahSU-ESJcvB6Ti421mcyUT9MyUIiPI1Bf7ZuADlDQafM_FZg16o2K22caATMVdbsB36Gl1",
            to: data.device_token,
            // this to: 'dSe867oxRSqzUbwzXHUiPk:APA91bH5eizc9xm1H6rERG8vjEviWcQongGlrsewM48iCG2K7UEnTD8gL_7sLlzeYZJ2gtmmslu6xeY8aWmHfFU5BrURitLqmgTadJ_ib_1cMuy-G2uUgF0EQY2GBeHdEhURHs8c6he0',
            // to: 'd0z-UMGIwtuKgHsBK7O650:APA91bFSFFux4x7NlKojf3ZjZQyxr2_wkSsLMLDr2fvM0urqAG4dDCqbfdv3B4L1-tNqyCDVWmhphxYnhdsC04LS5Ny1tlvm_S5YEfxsxVz5AnyaLECsORffH3ZPGFQWidsNqrooBv5J',
            //collapse_key: 'red', //for replaced with new one

            notification: {  //you can send only notification or only data(or include both)
                title: data.title,
                body: data.message,
            }
        }
    } else {
        var message = { //this may vary according to the message type (single recipient, multicast, topic, et cetera)
            to: data.device_token,
            // this    to: 'd0z-UMGIwtuKgHsBK7O650:APA91bFSFFux4x7NlKojf3ZjZQyxr2_wkSsLMLDr2fvM0urqAG4dDCqbfdv3B4L1-tNqyCDVWmhphxYnhdsC04LS5Ny1tlvm_S5YEfxsxVz5AnyaLECsORffH3ZPGFQWidsNqrooBv5J', to: 'dSe867oxRSqzUbwzXHUiPk:APA91bH5eizc9xm1H6rERG8vjEviWcQongGlrsewM48iCG2K7UEnTD8gL_7sLlzeYZJ2gtmmslu6xeY8aWmHfFU5BrURitLqmgTadJ_ib_1cMuy-G2uUgF0EQY2GBeHdEhURHs8c6he0',
            // to: 'd0z-UMGIwtuKgHsBK7O650:APA91bFSFFux4x7NlKojf3ZjZQyxr2_wkSsLMLDr2fvM0urqAG4dDCqbfdv3B4L1-tNqyCDVWmhphxYnhdsC04LS5Ny1tlvm_S5YEfxsxVz5AnyaLECsORffH3ZPGFQWidsNqrooBv5J',
            data: {  //you can send only notification or only data(or include both)
                title: data.title,
                message: data.message,
            }
        }
    }

    fcm.send(message, function (err, response) {
        console.log(message, "Data Line no. 2335");
        if (err) {
            console.log("fcm error", err)
            return true;
        } else {
            new fcmModel(data).save((saveErr, saveRes) => {

                if (saveErr) {
                    console.log(saveErr, "saveErr");
                    // return res.send({ responseCode: 500, responseMessage: "Internal server error.", responseResult: saveErr });
                }
                else {
                    console.log(saveRes, "saveRes");
                    // return res.send({ responseCode: 200, responseMessage: "Notification save successfully!", responseResult: saveRes });
                }
            })
            console.log("success", response)
            return true;
        }
    });
}

exports.addUserPlan = (req, res, next) => {
    let userVariable = {
        user_id: mongoose.Types.ObjectId(req.body.user_id),
        plan_id: mongoose.Types.ObjectId(req.body.plan_id),
        transectionId: req.body.transectionId,

    }
    new userPlanModel(userVariable).save().then(data => {
        res.status(200).json({ 'status': 'success', 'msg': 'Save userPlan Successfully', 'data': data });
    }).catch((error) => {
        console.log(error)
        res.status(200).json({ 'status': 'false', 'msg': 'something went wrong', 'data': error });
    });
}

exports.userPlanList = async (req, res, next) => {

    const planData = await userPlanModel.aggregate([
        {
            "$match": {
                "user_id": mongoose.Types.ObjectId(req.body.user_id)
            }
        },
        {
            "$project": {
                "userPlan": "$$ROOT"
            }
        },
        {
            "$sort": {
                "createdAt": -1
            }
        },
        {
            $lookup: {
                from: "subscriptions",
                localField: "userPlan.plan_id",
                foreignField: "_id",
                as: "subscription"
            }
        },
        {
            "$unwind": {
                "path": "$subscription",
                "preserveNullAndEmptyArrays": true
            }
        },
        {
            "$project": {
                "user_id": "$userPlan.user_id",
                "plan_id": "$userPlan.plan_id",
                "transectionId": "$userPlan.transectionId",
                "paymentMode": "$userPlan.paymentMode",
                "subcriptionDate": "$userPlan.createdAt",
                "expiryDate": "$subscription.validity",
                "validity": "$subscription.validity",
                "subscription_name": "$subscription.subscription_name",
                "charge": "$subscription.charge",
                "description": "$subscription.description"
            }
        },

    ]);

    res.status(200).json({ 'status': 'success', 'msg': 'Save userPlan List fetched successfully', 'data': planData });

}
exports.fcmList = function (req, res, next) {

    fcmModel.find({ userId: mongoose.Types.ObjectId(req.body.user_id) }).exec().then(data => {
        console.log(data, "data");
        if (data) {
            res.status(200).json({ 'status': 'success', 'msg': 'Fcm notification list', 'data': data });
        }
        else {
            res.status(200).json({ 'status': 'error', 'msg': 'No data available', 'data': [] });

        }
    })

}


exports.addUserPlan = (req, res, next) => {
    let userVariable = {
        user_id: mongoose.Types.ObjectId(req.body.user_id),
        plan_id: mongoose.Types.ObjectId(req.body.plan_id),
        transectionId: req.body.transectionId,

    }
    console.log(userVariable);
    new userPlanModel(userVariable).save().then(async data => {

        const updateSubscribe = await user.findByIdAndUpdate({ _id: mongoose.Types.ObjectId(data.user_id) }, { $set: { isSubscribe: true } }, { new: true });
        console.log(updateSubscribe, " updateSubscribe");

        res.status(200).json({ 'status': 'success', 'msg': 'Save userPlan Successfully', 'data': data });
    }).catch((error) => {
        console.log(error)
        res.status(200).json({ 'status': 'false', 'msg': 'something went wrong', 'data': error });
    });

}

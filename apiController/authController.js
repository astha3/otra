const mongoose = require('mongoose');
const user = require('../Models/users')
const bcrypt = require("bcrypt-nodejs");
const jwt = require('jsonwebtoken');
const teacher = require('../Models/teacher');
const login_logModel = require('../Models/login_log');
//------------------------------------admin-------------------------------------

exports.register = async function (req, res, next) {
    console.log(req.file)
    let userVariable = {
        "username": req.body.username,
        "password": req.body.password,
        "email": req.body.email,
        "phoneNumber": req.body.phoneNumber,
        "profileImage": req.file.filename,
        "role": req.body.role,
    }

    var users = new user(userVariable);

    users.save().then(function (doc) {
        if (doc.length > 0) {
            res.status(200).json({ status: true, msg: 'registration successfull', data: doc })
        }
        else {
            console.log(doc);
            res.status(200).json({ status: true, msg: 'registration successfull', data: doc })

        }
    }).catch(err => {
        res.status(200).json({ status: true, msg: 'something went wrong', data: null })

    })

}

exports.login = async function (req, res, next) {
    if (req.user) {
        res.redirect('/');
    } else {
        res.locals = { title: 'login' };
        res.render('Auth/login');
    }
}

//----------------------------------- app user------------------------------------------------

exports.userRegistration = async function (req, res, next) {
    //   console.log(req.body,'body')
    //  console.log(req.file)
    try {
        const post = await user.find({ role: 2 }).countDocuments();
        let count = 0;
        for (i = 0; i < post; i++) {
            count++
        }
        console.log(count, "line no 57============================>")
        var userVariable = {
            "schoolNumber": `School-${count}`,
            "username": req.body.username,
            "password": req.body.password,
            "email": req.body.email,
            "phoneNumber": req.body.phoneNumber,
            "role": req.body.role,
            "country_code": req.body.country_code,
            "address": req.body.address,
            "country": mongoose.Types.ObjectId(req.body.country),
            "state": mongoose.Types.ObjectId(req.body.state),
            "city": mongoose.Types.ObjectId(req.body.city),
            "device_type": req.body.device_type,
            "device_token": req.body.device_token,

        }
        console.log(userVariable, "line no 69============================>")
        if (req.body.role == 2) {
            if (req.file.filename) {
                userVariable.profileImage = req.file.filename
            }
        }

        if (req.body.role == 3) {

            if (req.file != undefined) {
                var teacherData = {
                    "resume": req.file.filename,
                    "subject": req.body.specialism,
                    "school_type": mongoose.Types.ObjectId(req.body.schoolType),
                }
            }
            else {
                var teacherData = {
                    "subject": req.body.specialism,
                    "school_type": mongoose.Types.ObjectId(req.body.schoolType),
                }
            }

        }
        // else {
        //     var userVariable = {
        //         address: req.body.address,
        //         email: req.body.email,
        //         firstname: req.body.firstname,
        //         fromHere: mongoose.Types.ObjectId(req.body.fromHere),
        //         lastname: req.body.lastname,
        //         password: req.body.password,
        //         schoolName: req.body.schoolName,
        //         phoneNumber: req.body.phoneNumber,
        //         role: 2,
        //     }
        // }
        user.count({ email: req.body.email }).exec(function (err, doc) {
            if (err) {
                res.status(200).json({ 'status': 'error', 'msg': 'something went wrong', 'data': null });

            }
            else {
                if (doc > 0) {
                    res.status(200).json({ 'status': 'error', 'msg': 'email already registered', 'data': null });

                }
                else {
                    user.count({ phoneNumber: req.body.phoneNumber }).exec().then(function (numberCount) {
                        console.log(doc, numberCount)
                        if (numberCount > 0) {
                            res.status(200).json({ 'status': 'error', 'msg': 'phone number already registred', 'data': null });

                        }
                        else {
                            console.log(userVariable < "Line no 122++++++++++++++++++++++=")
                            var saveAppUser = new user(userVariable);
                            console.log(saveAppUser, "line no 126===========================>")
                            saveAppUser.save().then(function (data) {
                                console.log(data < "after save Line no 127++++++++++>")
                                if (req.body.role == 3) {
                                    teacherData.teacher_id = data._id;
                                    new teacher(teacherData).save(async (err, teacherData) => {
                                        if (err) {
                                            console.log(err)
                                            res.status(200).json({ 'status': 'error', 'msg': 'something went wrong', 'data': [] });

                                        }
                                        else {
                                            var getAllData = await user.aggregate([
                                                {
                                                    "$match": {
                                                        _id: mongoose.Types.ObjectId(teacherData.teacher_id)
                                                    }
                                                },
                                                {
                                                    "$project": {
                                                        "ca": "$$ROOT"
                                                    }
                                                },
                                                {
                                                    $lookup: {
                                                        from: "teachers",
                                                        localField: "ca._id",
                                                        foreignField: "teacher_id",
                                                        as: "teacher"
                                                    }
                                                },
                                                {
                                                    "$unwind": {
                                                        "path": "$teacher",
                                                        "preserveNullAndEmptyArrays": true
                                                    }
                                                },
                                                {
                                                    $lookup: {
                                                        from: "school_types",
                                                        localField: "teacher.school_type",
                                                        foreignField: "_id",
                                                        as: "school"
                                                    }
                                                },
                                                {
                                                    "$unwind": {
                                                        "path": "$school",
                                                        "preserveNullAndEmptyArrays": true
                                                    }
                                                },

                                                {
                                                    "$project": {
                                                        "teacher_id": "$teacher.teacher_id",
                                                        "username": "$ca.username",
                                                        "email": "$ca.email",
                                                        "phoneNumber": "$ca.phoneNumber",
                                                        "countrycode": "$ca.country_code",
                                                        "address": "$ca.address",
                                                        "school_type_id": "$school._id",
                                                        "school_type_name": "$school.type",
                                                        "subject_name": "$teacher.subject",
                                                        "skills": "$teacher.skills",
                                                        "resume": "$ca.resume",
                                                        "country": "$ca.country",
                                                        "state": "$ca.state",
                                                        "city": "$ca.city",

                                                    }
                                                }
                                            ])

                                            res.status(200).json({ 'status': 'success', 'msg': 'Registration Successfully', 'data': getAllData });
                                        }
                                    });
                                }
                                else {
                                    user.find({ _id: mongoose.Types.ObjectId(data._id) }).exec((err, responseData) => {
                                        console.log(responseData, "line no 216")

                                        if (err) {
                                            res.status(200).json({ 'status': 'error', 'msg': 'something went wrong', 'data': err });

                                        }
                                        else {
                                            res.status(200).json({ 'status': 'success', 'msg': 'Registration Successfully', 'data': responseData });

                                        }
                                    })
                                }
                            })
                        }
                    })
                }
            }
        })
    }
    catch (err) {
        console.log(err)
        res.status(200).json({ 'status': 'error', 'msg': 'something went wrong', 'data': null });
    }
}

const url = {
    base_url: 'http://182.76.237.237:9000/upload'
};

exports.appLogin = async function (req, res, next) {
    const type = req.body.type;

    if (req.body.type == 1) {
        const google_id = req.body.google_id;
        const json = {
            "google_id": req.body.google_id,
            "type": req.body.type,
            "role": req.body.role,
            "email": req.body.email
        }

        var getUser = await user.find({ google_id: req.body.google_id }).exec();
        console.log(getUser, "getUser line no 253");

        if (getUser.length <= 0) {
            var getUserEmail = await user.find({ email: req.body.email }).exec();
            if (getUserEmail.length <= 0) {
                const newUser = await user(json).save();
                console.log(newUser._id, "neweUser");

                var token = jwt.sign({ user: json }, 'secretkey', { expiresIn: '20d' });
                console.log(token, "token");

                let device_token = req.body.device_token;
                let device_type = req.body.device_type;

                var userDataLogin = await login_logModel.find({ user_id: mongoose.Types.ObjectId(newUser._id) }).exec();
                if (userDataLogin.length <= 0) {
                    var log_data = { 'user_id': mongoose.Types.ObjectId(newUser._id), "device_token": device_token, "device_type": device_type }
                    var login = new login_logModel(log_data);
                    login.save()
                }
                else {
                    await login_logModel.deleteMany({ 'user_id': mongoose.Types.ObjectId(newUser._id) })
                    var log_data = { 'user_id': mongoose.Types.ObjectId(newUser._id), "device_token": device_token, "device_type": device_type }
                    var login = new login_logModel(log_data);
                    // console.log(login, "login line no 289");
                    login.save()
                }

                var data = {
                    userDetail: newUser,
                    token: token,
                    fcm_login_log: log_data,
                }

                res.status(200).json({ 'status': 'success', 'msg': 'successfuly login', 'data': data })

                // res.status(200).json({ 'status': 'success', 'msg': "login successfully", 'data': newUser });
            } else {
                res.status(200).json({ 'status': 'success', 'msg': 'user already exists ', 'data': [] });
            }
        }
        else {

            var editUserData = await user.findOneAndUpdate({ google_id: req.body.google_id }, json, { new: true });
            console.log(editUserData, "editUserData line no 297");

            var token = jwt.sign({ user: json }, 'secretkey', { expiresIn: '20d' });
            console.log(token, "token");

            let device_token = req.body.device_token;
            let device_type = req.body.device_type;

            var userDataLogin = await login_logModel.find({ user_id: mongoose.Types.ObjectId(editUserData._id) }).exec();
            if (userDataLogin.length <= 0) {
                var log_data = { 'user_id': mongoose.Types.ObjectId(editUserData._id), "device_token": device_token, "device_type": device_type }
                var login = new login_logModel(log_data);
                login.save()
            }
            else {
                await login_logModel.deleteMany({ 'user_id': mongoose.Types.ObjectId(editUserData._id) })
                var log_data = { 'user_id': mongoose.Types.ObjectId(editUserData._id), "device_token": device_token, "device_type": device_type }
                var login = new login_logModel(log_data);
                console.log(login, "login line no 315");
                login.save()
            }

            var data = {
                userDetail: editUserData,
                token: token,
                fcm_login_log: log_data,
            }

            res.status(200).json({ 'status': 'success', 'msg': 'successfuly login', 'data': data });

            // res.status(200).json({ 'status': 'success', 'msg': 'successfuly login', 'data': editUserData })
        }
    }
    else if (req.body.type == 2) {
        const facebook_id = req.body.facebook_id;
        const json = {
            "facebook_id": req.body.facebook_id,
            "type": req.body.type,
            "role": req.body.role,
            "email": req.body.email
        }
        var getUser = await user.find({ facebook_id: (req.body.facebook_id) }).exec();
        console.log(getUser, "getUser line no 340");
        if (getUser.length <= 0) {
            var getUserEmail = await user.find({ email: (req.body.email) }).exec();
            if (getUserEmail.length <= 0) {
                console.log("yesssssssssssssssssssssssssssssssssss");
                const newUser = await user(json).save();

                var token = jwt.sign({ user: json }, 'secretkey', { expiresIn: '20d' });
                console.log(token, "token line no 348");

                let device_token = req.body.device_token;
                let device_type = req.body.device_type;

                var userDataLogin = await login_logModel.find({ user_id: mongoose.Types.ObjectId(newUser._id) }).exec();
                console.log(userDataLogin, "userDataLogin line no 354")
                if (userDataLogin.length <= 0) {
                    var log_data = { 'user_id': mongoose.Types.ObjectId(newUser._id), "device_token": device_token, "device_type": device_type }
                    var login = new login_logModel(log_data);
                    login.save()
                }
                else {
                    await login_logModel.deleteMany({ 'user_id': mongoose.Types.ObjectId(newUser._id) })
                    var log_data = { 'user_id': mongoose.Types.ObjectId(newUser._id), "device_token": device_token, "device_type": device_type }
                    var login = new login_logModel(log_data);
                    console.log(login, "login line no 289");
                    login.save()
                }

                var data = {
                    userDetail: newUser,
                    token: token,
                    fcm_login_log: log_data,
                }

                res.status(200).json({ 'status': 'success', 'msg': 'successfuly login', 'data': data })

                // res.status(200).json({ 'status': 'success', 'msg': "login successfully", 'data': newUser });
            } else {
                res.status(200).json({ 'status': 'success', 'msg': 'user already exists ', 'data': [] });
            }
        }
        else {
            console.log("else part")
            var editUserData = await user.findOneAndUpdate({ facebook_id: req.body.facebook_id }, json, { new: true });
            console.log(editUserData, "editUserData facebook line no 385")
            var token = jwt.sign({ user: json }, 'secretkey', { expiresIn: '20d' });
            console.log(token, "token line no 388");

            let device_token = req.body.device_token;
            let device_type = req.body.device_type;

            var userDataLogin = await login_logModel.find({ user_id: mongoose.Types.ObjectId(editUserData._id) }).exec();
            if (userDataLogin.length <= 0) {
                var log_data = { 'user_id': mongoose.Types.ObjectId(editUserData._id), "device_token": device_token, "device_type": device_type }
                var login = new login_logModel(log_data);
                console.log(login, "login line no 397");
                login.save()
            }
            else {
                await login_logModel.deleteMany({ 'user_id': mongoose.Types.ObjectId(editUserData._id) })
                var log_data = { 'user_id': mongoose.Types.ObjectId(editUserData._id), "device_token": device_token, "device_type": device_type }
                var login = new login_logModel(log_data);
                console.log(login, "login line no 403");
                login.save()
            }

            var data = {
                userDetail: editUserData,
                token: token,
                fcm_login_log: log_data,
            }

            res.status(200).json({ 'status': 'success', 'msg': 'successfuly login', 'data': data })

            //            res.status(200).json({ 'status': 'success', 'msg': 'successfuly login', 'data': editUserData })
        }

    } else {
        console.log("+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ else part")
        var userVariable = {
            email: req.body.email,
            password: req.body.password
        }

        let device_token = req.body.device_token;
        let device_type = req.body.device_type;

        user.find({ email: req.body.email }).exec().then(doc => {
            console.log(doc, "doc line no 250")
            doc1 = doc.map(obj => {

                if (obj.profileImage) {

                    obj.profileImage = `${url.base_url}/${obj.profileImage}`;   // obj.profileImage;
                }
                else {
                    obj.profileImage = '';
                }
                return obj;
            })
            if (doc1.length > 0) {
                if (doc1[0].role == 2 && !doc1[0].approve) {
                    res.status(200).json({ 'status': 'error', 'msg': 'please get approval from admin', 'data': null })
                }
                else {
                    // if (doc1[0].role == 3 && doc1[0].isSubscribe == false) {
                    //     res.status(200).json({ 'status': 'error', 'msg': 'Please first Subscribe plan', 'data': null })
                    // } else {

                        if (doc1[0].status) {
                            let isMatch = bcrypt.compareSync(req.body.password, doc1[0].password);
                            if (isMatch) {
                                jwt.sign({ user: userVariable }, 'secretkey', { expiresIn: '20d' }, async (err, token) => {
                                    if (doc1[0].profilePic) {
                                        doc1[0].profilePic = `${url.base_url}/${doc[0].profilePic}`
                                    }

                                    let getTeacherDetail = await teacher.find({ teacher_id: mongoose.Types.ObjectId(doc1[0]._id) }).exec();

                                    var userDataLogin = await login_logModel.find({ user_id: mongoose.Types.ObjectId(doc1[0]._id) }).exec();
                                    // console.log(userDataLogin, "userDataLogin");
                                    if (userDataLogin.length <= 0) {
                                        var log_data = { 'user_id': mongoose.Types.ObjectId(doc1[0]._id), "device_token": device_token, "device_type": device_type }
                                        var login = new login_logModel(log_data);
                                        // console.log(login, "login line no 281");
                                        login.save()
                                    }
                                    else {
                                        await login_logModel.deleteMany({ 'user_id': mongoose.Types.ObjectId(doc1[0]._id) })
                                        var log_data = { 'user_id': mongoose.Types.ObjectId(doc1[0]._id), "device_token": device_token, "device_type": device_type }
                                        var login = new login_logModel(log_data);
                                        // console.log(login, "login line no 289");
                                        login.save()
                                    }

                                    var json = {
                                        userDetail: doc1[0],
                                        getTeacherDetail: getTeacherDetail[0],
                                        token: token
                                    }

                                    res.status(200).json({ 'status': 'success', 'msg': 'successfuly login', 'data': json })
                                });
                            }
                            else {
                                res.status(200).json({ 'status': 'error', 'msg': 'password incorrect', 'data': null })
                            }
                        }
                        else {
                            res.status(200).json({ 'status': 'error', 'msg': 'user blocked by admin', 'data': null })
                        }
                    }
               // }
            }
            else {
                res.status(200).json({ 'status': 'error', 'msg': 'email id not exist', 'data': null })
            }
        })
    }
}

exports.test = async function (req, res, next) {
    await user.update({ _id: mongoose.Types.ObjectId('61337575df6f19dd37a3382d') }, { password: req.body.password }).exec().then(data => {
        res.status(200).json(data)

    });
}

exports.error = async function (req, res, next) {
    res.locals = { title: '404' };
    var profileImage = req.user.profileImage;
    res.status(404);
    res.render('Admin/pages_401', { profileImage })
}



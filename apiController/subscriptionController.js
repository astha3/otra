const mongoose = require('mongoose');
const user = require('../Models/users');
const moment = require('moment');
const subscription=require('../Models/subscriptionControl')
const roles=require('../Models/role')

exports.addSubscription=async function(req,res,next){
    var userVariable={
        subscription_name:req.body.subscription_name,
        validity:req.body.validity,
        charge:req.body.charge,
        description:req.body.description
    }
new subscription(userVariable).save().then(data=>{
    req.flash('success','subscription plan added succesfully')
    res.redirect('/subscription_list')
}).catch(err=>{
    console.log(err)
    req.flash('error','something went wrong')

    res.redirect('/subscription_list')
})

}

exports.subscription_list=async function(req,res,next){
    subscription.find({}).sort( { createdAt: -1 } ).exec().then(data=>{
        console.log(data,'++++++++++++++++')
        res.locals = { title: 'Subscription Plan' };
        var profileImage = req.user.profileImage;
        getUserDetails(req.user._id, function (result) {
            console.log(result)
            if(req.user.role === 1){
                res.render('Admin/subscription',{data,moment,profileImage}) 
            }else if(result.subscriptionPlan === true) {
                res.render('Admin/subscription',{data,moment,profileImage}) 
            }else if(result.subscriptionPlan === false) {
                res.render('Admin/permission_error',{profileImage});
            }
        });
    }).catch(err=>{
        console.log(err,'error')
       res.send(500);
    })
}

function getUserDetails(roleId,callback){
    var user = {};
    roles.find({user_id:mongoose.Types.ObjectId(roleId)})
        .exec(function(err, roles) {
            if (err) { return next(err); }
            callback(roles[0]);
        });
}

exports.deletePlan=async function(req,res,next){
    subscription.deleteOne({_id:mongoose.Types.ObjectId(req.params.id)}).then(data=>{
        res.redirect('/subscription_list')
    }).catch(err=>{
        console.log(err,'error in deletePlan')
        res.redirect('/subscription_list')

    })
}

exports.planDetail=async function(req,res,next){
    subscription.find({_id:mongoose.Types.ObjectId(req.body.id)}).exec().then(data=>{
        res.send(data)
    }).catch(err=>{
        res.send(err)
    })
}
const mongoose = require('mongoose');
const user = require('../Models/users')
const Roles = require('../Models/role');
const role = require('../Models/role');
const bcrypt = require('bcrypt-nodejs');
const roles = require('../Models/roles');
var SALT_FACTOR = 10;
var noop = function () { };

exports.profile = async function (req, res, next) {
    try {
        var profileImage = req.user.profileImage;
        let getUser = await user.find({ _id: mongoose.Types.ObjectId(req.user._id) }).exec();
        res.locals = { title: 'Profile' };
        res.render('Admin/profile', { getUser: getUser, profileImage });
    } catch (err) {
        res.send(500)
    }
}

exports.editProfile = async function (req, res, next) {
    console.log(req.body, req.file, 'editProfile');
    var profileImage = req.user.profileImage;
    var userVariable = {};
    if (req.file == ' ' || req.file == null) {
        userVariable = {
            username: req.body.username,
            email: req.body.email,
            phoneNumber: req.body.number,
            password: req.body.password
        }
    }
    else {
        userVariable = {
            username: req.body.username,
            email: req.body.email,
            phoneNumber: req.body.number,
            password: req.body.password,
            profileImage: req.file.filename
        }
    }
    var editUserData = await user.update({ _id: mongoose.Types.ObjectId(req.user._id) }, userVariable)
    var getUser = await user.find({ _id: mongoose.Types.ObjectId(req.user._id) }).exec();
    console.log(getUser[0].profileImage, "mekfoihf bfi3f line no 44================================>")
    if (editUserData.length > 0) {
        console.log(editUserData, 'fdsdsdfdsf');
        res.locals = { title: 'Profile' };
        res.render('Admin/profile', { getUser, profileImage });
    }
    else {
        console.log(editUserData, 'dksdklsfdlkdslkdslkdlskldkflsdkkl')
        res.locals = { title: 'Profile' };
        res.render('Admin/profile', { getUser, profileImage });
    }


}

exports.subadminList = async function (req, res, next) {
    var profileImage = req.user.profileImage;

    var subadminUserList = await user.find({ role: 5 }).exec();

    res.locals = { title: 'Subadmin' };
    console.log(subadminUserList);
    
    getUserDetails(req.user._id, function (result) {
        console.log(result)
        if (req.user.role === 1) {
            res.render('Admin/subadmin', { getUser: subadminUserList, profileImage });
        } else if (result.subAdmin === true) {
            res.render('Admin/subadmin', { getUser: subadminUserList, profileImage });
        } else if (result.subAdmin === false) {
            res.render('Admin/permission_error', { profileImage });
        }
    });



}

exports.addSubadmin = async function (req, res, next) {
    console.log(req.file, 'dssdsdsdf')

    var json = {
        username: req.body.username,
        password: req.body.password,
        email: req.body.email,
        phoneNumber: req.body.number,
        role: 5
    }
    if (req.file) {
        json.profileImage = req.file.filename
    }

    user.count({ "$or": [{ email: req.body.email }, { phoneNumber: req.body.number }] }).exec().then(data => {
        console.log(data, 'count ++++++')

        if (data < 1) {
            let addSubadmin = new user(json);
            addSubadmin.save().then(async function (data) {
                console.log(data, 'here');
                try {
                    let subadminData = await new role({ user_id: mongoose.Types.ObjectId(data._id), updatedby: mongoose.Types.ObjectId(req.user._id) }).save()
                    console.log(subadminData, 'subadminData')
                    req.flash('success', 'subadmin added succesfuly');
                    res.redirect('/subadmin')
                }
                catch (err) {
                    req.flash('error', 'added new subadmin');

                    console.log(err, 'error is here added subadmin');
                    res.redirect('/subadmin')
                }
            }).catch(err => {
                req.flash('error', err);

                res.redirect('/subadmin')
            });
        }
        else {
            req.flash('error', 'email or number already registered ');

            res.redirect('/subadmin')
        }
    }).catch(err => {
        req.flash('error', 'something went wrong');

        console.log(err, 'error is here added subadmin');
        res.redirect('/subadmin')
    })

}

exports.editSubadminDetail = async function (req, res, next) {
    console.log(req.query.id)
    let editDetails = await user.find({ _id: mongoose.Types.ObjectId(req.query.id) }).exec()
    res.send({ editDetails: editDetails[0] })

}

exports.saveEditSubadmin = async function (req, res, next) {
    var profileImage = req.user.profileImage;

    if (req.file == " " || req.file == undefined) {
        var json = {
            username: req.body.username,
            email: req.body.email,
            phoneNumber: req.body.number,
        }
    }
    else {
        var json = {
            username: req.body.username,
            email: req.body.email,
            phoneNumber: req.body.number,
            profileImage: req.file.filename,
        }
    }

    user.updateOne({ _id: mongoose.Types.ObjectId(req.body.editId) }, { $set: json }).then(async function (data) {
        req.flash('success', 'updated subadmin');

        res.redirect('/subadmin')
    }).catch(err => {
        req.flash('error', 'updated subadmin');

        res.redirect('/subadmin')
    })
}

exports.permissionUser = async function (req, res, next) {
    var module = req.query.module;
    var input = {
        updatedby: req.user._id,
    };
    input[module] = req.query.value;
    var options = {
        new: true,
        upsert: true,
        //setDefaultsOnInsert: true
    };
    Roles.update({ user_id: mongoose.Types.ObjectId(req.query.user_id) },
        input, options)
        .then(function (docs) {
            res.status(200).json({
                collections: docs
            });
            //console.log(docs)
        }).catch(function (err) {
            console.log(err);
            res.status(500).json({
                error: err
            });
            //console.log(err)

        });
}

exports.assignRole = async function (req, res, next) {
    res.locals = { title: 'permission' };
    var profileImage = req.user.profileImage;
    var usersList = await user.find({ _id: mongoose.Types.ObjectId(req.params.id) }).exec();
    var userRoleDetail = await role.find({ user_id: usersList[0]._id }).exec();
    res.render('Admin/permission', { profileImage, usersList, userRoleDetail });
}
exports.changePassword = async function (req, res, next) {
    var profileImage = req.user.profileImage;

    user.find({ email: req.user.email }).exec().then(data => {
        if (data.length > 0) {
            if (req.body.old_password) {
                user.comparePassword(req.body.old_password, data[0].password, function (err, isMatch) {
                    if (isMatch) {
                        bcrypt.genSalt(SALT_FACTOR, function (err, salt) {
                            if (err) {
                                console.log(err);
                                return done(err);
                            }
                            bcrypt.hash(req.body.pwd1, salt, noop, function (err, hashedPassword) {
                                if (err) { return done(err); }

                                user.password = hashedPassword;
                                var Variables = {
                                    password: user.password,
                                };
                                console.log(Variables, hashedPassword, req.body.pwd1, salt, noop);
                                user.update({ _id: mongoose.Types.ObjectId(req.user._id) }, Variables, function (err, raw) {
                                    res.locals = { title: 'change Password' };
                                    res.render('Admin/changePassword', { profileImage });
                                })
                            });
                        });
                    }
                    else {
                        res.locals = { title: 'change Password' };
                        res.render('Admin/changePassword', { profileImage });
                    }
                })
            }
            else {
                bcrypt.genSalt(SALT_FACTOR, function (err, salt) {
                    if (err) {
                        console.log(err);
                        return done(err);
                    }
                    bcrypt.hash(req.body.pwd1, salt, noop, function (err, hashedPassword) {
                        if (err) { return done(err); }

                        let password = hashedPassword;
                        var Variables = {
                            password: password,
                        };
                        user.update({ _id: mongoose.Types.ObjectId(req.user._id) }, Variables, function (err, raw) {
                            if (err) {
                                console.log(err)
                                res.locals = { title: 'login' };
                                res.render('Auth/login', { profileImage });
                            }
                            else {
                                res.locals = { title: 'login' };
                                res.render('Auth/login', { profileImage });
                            }
                        })
                    });
                });
            }
        }
    })
}

exports.changePasswordPage = function (req, res, next) {
    var profileImage = req.user.profileImage;
    res.locals = { title: 'change Password' };
    res.render('Admin/changePassword', { profileImage });
}


function generateOTP() {
    var digits = '0123456789';
    let OTP = "";
    for (let i = 0; i < 4; i++) {
        OTP += digits[Math.floor(Math.random() * 10)];
    }
    return Number(OTP);
}

exports.sendOtp = function (req, res, next) {
    console.log(req.body)
    var username = req.body.email
    res.locals = { title: 'change Password' };

    if (isNaN(username)) {
        var search = { email: username };
    } else {
        var search = { phoneNumber: username };
    }

    user.find(search)
        .exec(function (err, userData) {
            console.log(userData)
            if (err) {
                console.log(err)
            } else {
                if (userData.length > 0) {
                    let otp = generateOTP();
                    var upddata = {
                        'otp': otp,
                        'username': userData[0].email,
                    }
                    console.log(upddata, 'fgfghfghhg')
                    res.render('Auth/pages_lock_screen')
                } else {
                    res.render('Auth/pages_lock_screen')
                }
            }
        })
}


exports.deleteSubadmin = function (req, res, next) {
    role.deleteOne({ user_id: mongoose.Types.ObjectId(req.params.id) }).exec().then(data => {
        user.deleteOne({ _id: mongoose.Types.ObjectId(req.params.id) }).exec().then(user => {
            req.flash('success', 'deleted Subadmin');

            res.redirect('/subadmin')
        }).catch(err => {
            req.flash('error', 'something went wrong');

            res.redirect('/subadmin')

        })
    }).catch(err => {
        req.flash('error', 'something went wrong');

        res.redirect('/subadmin')

    })

}

function getUserDetails(roleId, callback) {
    var user = {};
    role.find({ user_id: mongoose.Types.ObjectId(roleId) })
        .exec(function (err, roles) {
            if (err) { return next(err); }
            callback(roles[0]);
        });
}
const mongoose = require('mongoose');
const user = require('../Models/users')
const bcrypt = require("bcrypt-nodejs");
const jwt = require('jsonwebtoken');
const teacher = require('../Models/teacher');
const roles=require('../Models/role')

exports.dashboard=async function(req,res,next){
  var newSchool=await user.count({role:2,approve:false}).exec();  
  var school=await user.count({role:2,approve:true}).exec();
  var teacher=await user.count({role:3}).exec();
  res.locals = {  title: 'Dashboard' };
  var profileImage=req.user.profileImage;
  getUserDetails(req.user._id, function (result) {
    console.log(result)
    if(req.user.role === 1){
      res.render('Dashboard/dashboard',{newSchool,school,teacher,profileImage})
    }else if(result.dashboard === true) {
      res.render('Dashboard/dashboard',{newSchool,school,teacher,profileImage})
    }else if(result.dashboard === false) {
        res.render('Admin/permission_error',{profileImage});
    }
});
}

function getUserDetails(roleId,callback){
  var user = {};
  roles.find()
      .where('user_id').equals(roleId)
      .exec(function(err, roles) {
          if (err) { return next(err); }
          callback(roles[0]);
      });
}
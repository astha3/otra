const mongoose = require('mongoose');
const user = require('../Models/users');
const moment = require('moment');
const { userRegistration } = require('./authController');
const teacher = require('../Models/teacher');
const experience = require('../Models/experience')
const subject = require('../Models/subject')
const schoolType = require('../Models/schoolType')
const roles=require('../Models/role')

exports.teacherList = async function (req, res, next) {
    console.log('get api teacher')
    try {
        user.aggregate([
            {
                "$match": {
                    "role": 3
                },

            },

            {
                "$project": {
                    "ca": "$$ROOT"
                }
            },
            {
                "$sort": {
                    "ca.createdAt": -1
                }
            },
            {
                $lookup: {
                    from: "teachers",
                    localField: "_id",
                    foreignField: "teacher_id",
                    as: "teacher"
                }
            },
            {
                "$unwind": {
                    "path": "$teacher",
                    "preserveNullAndEmptyArrays": true
                }
            },
           
            {
                "$project": {
                    "email": "$ca.email",
                    "phoneNumber": "$ca.phoneNumber",
                    "country_code": "$ca.country_code",
                    "address": "$ca.address",
                    "resume": "$teacher.resume",
                    "experience": "$teacher.experience",
                    "username": "$ca.username",
                    "status": "$ca.status"
                }
            }
        ]).then(async data => {
            console.log(data)
            res.locals = { title: 'Teacher' };
            var profileImage = req.user.profileImage;
            var experienceData = await experience.find({}).exec();
            var subjectData = await subject.find({}).exec();
            var schoolTypeData = await schoolType.find({}).exec()
            getUserDetails(req.user._id, function (result) {
                console.log(result)
                if(req.user.role === 1){
                    res.render('Admin/teacherList', { profileImage, data, experienceData, subjectData, schoolTypeData, moment });
                }else if(result.teacher === true) {
                    res.render('Admin/teacherList', { profileImage, data, experienceData, subjectData, schoolTypeData, moment });
                }else if(result.teacher === false) {
                    res.render('Admin/permission_error',{profileImage});
                }
            });
      
        })
    }
    catch (err) {
        console.log(err, 'serroe')
    }

    // user.find({role:3}).exec((err,result)=>{
    //     if(err){
    //         res.status(200).json({ 'status': 'error', 'msg': 'something went wrong ', 'data': null });

    //     }
    //     else{
    //         res.status(200).json({ 'status': 'success', 'msg': 'data successful', 'data': result });

    //     }
    // })
}

exports.deleteCandidate = async function (req, res) {
    console.log(req.query.id,req.params.url,'reqest');
    try {
        var url=req.params.url;

        var data = await user.deleteOne({ _id: mongoose.Types.ObjectId(req.query.id) });
        console.log(data);
        if (data.deletedCount > 0) {
            var teacherData = await teacher.deleteOne({ teacher_id: mongoose.Types.ObjectId(req.query.id) });
            req.flash('success', 'success')
            res.redirect(`../${url}`)
        }
        else {
            req.flash('success', 'success')

            res.redirect(`../${url}`)
        }
    }
    catch (err) {
        console.log('error', err)
        res.redirect(`../${url}`)

    }


}



exports.newSchool = async function (req, res, next) {
    user.find({ role: 2, 'approve': 'false' }).exec((err, data) => {
        console.log(data)
        res.locals = { title: 'School' };
        var profileImage = req.user.profileImage;
        getUserDetails(req.user._id, function (result) {
            console.log(result)
            if(req.user.role === 1){
                res.render('Admin/schoolList', { profileImage, data, moment });
            }else if(result.school === true) {
                res.render('Admin/schoolList', { profileImage, data, moment });
            }else if(result.school === false) {
                res.render('Admin/permission_error',{profileImage});
            }
        });
    })
}

exports.approved = async function (req, res, next) {
    try {
        user.update({ _id: mongoose.Types.ObjectId(req.query.id) }, { $set: { 'approve': true } }).then(data => {
            console.log(data);
            res.redirect('/newSchool')

        });
    }
    catch (err) {
        console.log(err, 'error')
        res.redirect('/newSchool')
    }
}

exports.School = async function (req, res, next) {
    user.find({ role: 2, 'approve': 'true' }).exec((err, data) => {
        console.log(data)
        res.locals = { title: 'School' };
        var profileImage = req.user.profileImage;
        getUserDetails(req.user._id, function (result) {
            console.log(result)
            if(req.user.role === 1){
                res.render('Admin/school', { profileImage, data, moment });
            }else if(result.school === true) {
                res.render('Admin/school', { profileImage, data, moment });
            }else if(result.school === false) {
                res.render('Admin/permission_error',{profileImage});
            }
        });
    })
}




exports.editUser = function (req, res, next) {
    console.log(req.body, 'edit');
    user.find({ _id: mongoose.Types.ObjectId(req.body.id) }).exec((err, data) => {

        if (err) {
            res.status(200).json({ 'status': 'error', 'msg': 'something went wrong', 'data': [] });

        }
        else {
            if (data.length > 0) {
                var userVariable = {
                    "username": req.body.username,
                    "email": req.body.email,
                    "phoneNumber": req.body.phoneNumber,
                    "country_code": req.body.country_code,
                    "address": req.body.address,
                }
                if (data[0].role == 3) {
                    var teacherData = {
                        "subject": mongoose.Types.ObjectId(req.body.subject),
                        "school_type": mongoose.Types.ObjectId(req.body.school_type),
                        "skills": req.body.skills,
                        "experience": mongoose.Types.ObjectId(req.body.experience),
                        "education_detail": req.body.educationDetails

                    }
                    if (req.files.file != undefined) {
                        teacherData.resume = req.files.file[0].filename;
                    }
                    if (req.files.profile != undefined) {
                        userVariable.profilePic = req.files.profile[0].filename;
                    }
                }
           


                console.log(userVariable)
                user.update({ _id: mongoose.Types.ObjectId(req.body.id) }, userVariable, function (err, data) {

                    if (err) {
                        console.log(err)
                        res.status(200).json({ 'status': 'error', 'msg': 'something went wrong', 'data': [] });

                    }
                    else {
                        teacher.updateOne({ teacher_id: mongoose.Types.ObjectId(req.body.id) }, teacherData, function (err, updatedTeacher) {
                            if (err) {
                                res.redirect('/teacherList')
                            }
                            else {
                                res.redirect('/teacherList')
                            }

                        })
                    }



                })
            }
            else {
                res.redirect('/teacherList')

            }
        }

    })

}



exports.getUserDetail = function (req, res, next) {
    user.find({ _id: mongoose.Types.ObjectId(req.query.candidate_id) }).exec((err, data) => {
        console.log(data);
        if (data[0].role == 3) {
            teacher.find({ teacher_id: mongoose.Types.ObjectId(data[0]._id) }).populate('subject', 'subject').populate('experience', 'experience').populate('school_type', 'type').exec((err, teacherData) => {

                var json = {
                    data: data[0],
                    teacherData: teacherData[0]

                }

                res.send(json)

            })
        }
        else {
            res.send(data[0])
        }
    })
}

exports.editSchool = function (req, res, next) {
    console.log(req.body, 'dkdflkjfdldjflfdjlkdfjldfkj');
    var user_id = req.body.id;
    var userVariable = {
        username: req.body.username,
        email: req.body.email,
        phoneNumber: req.body.number,
        country_code: req.body.country_code,
        address: req.body.address

    }
    if (req.file) {
        userVariable.profileImage = req.file.filename
    }
    console.log(userVariable, 'hjghhjjhjgh')
    user.find({ phoneNumber: req.body.number, role: 2 }).exec(async (err, data) => {
        if (err) {
            console.log(err)
            res.redirect('/School');
        }
        else {
            if (data.length > 0) {
                if (data[0]._id == user_id) {
                   
                    console.log('matched')
                    await user.updateOne({ _id: mongoose.Types.ObjectId(user_id) }, { $set: userVariable })
                    res.redirect('/school')

                }
                else {
                    console.log('notmatched')

                    res.redirect('/school')

                }
            }
            else {
                await user.update({ _id: mongoose.Types.ObjectId(user_id) }, { $set: userVariable })
                res.redirect('/school')

            }
        }
    })
}

exports.updateUserStatus = function (req, res, next) {
    console.log(req.query.id, req.params.status)
    var url=req.params.url;
    if (req.params.status == 1) {
        console.log("true")
        var status = true;
    }
    else {
        console.log("false")

        var status = false;

    }

    user.updateOne({ _id: mongoose.Types.ObjectId(req.query.id) }, { $set: { status } }, function (err, docs) {
        if (err) {
            console.log(err)

        }
        else {
            console.log("Updated Docs : ", docs);
            res.redirect(`/${url}`)

        }
    })
}

function getUserDetails(roleId,callback){
    var user = {};
    roles.find({user_id:mongoose.Types.ObjectId(roleId)})
        .exec(function(err, roles) {
            if (err) { return next(err); }
            callback(roles[0]);
        });
}


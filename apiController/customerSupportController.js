const mongoose = require('mongoose');
const user = require('../Models/users');
const moment = require('moment');
const { userRegistration } = require('./authController');
const query=require('../Models/query');
const feedback=require('../Models/feedback')

const roles=require('../Models/role')


exports.queryList=async function(req,res,next){
    query.find({}).populate('user_id').exec().then(data=>{
        var profileImage = req.user.profileImage;
        console.log(data,'jsjjsjsj')
        res.locals = { title: 'QUERY' };
        getUserDetails(req.user._id, function (result) {
            console.log(result)
            if(req.user.role === 1){
                res.render('Admin/query', { profileImage, data, moment });
            }else if(result.dashboard === true) {
                res.render('Admin/query', { profileImage, data, moment });
            }else if(result.dashboard === false) {
                res.render('Admin/permission_error',{profileImage});
            }
        });
    }).catch(error=>{
        throw new error
    })
}

exports.feedbackList=function(req,res,next){
    feedback.find({}).populate('user_id').exec().then(data=>{
        var profileImage = req.user.profileImage;
        console.log(data,'jsjjsjsj')
        res.locals = { title: 'School' };
        getUserDetails(req.user._id, function (result) {
            console.log(result)
            if(req.user.role === 1){
                res.render('Admin/feedback', { profileImage, data, moment });
            }else if(result.dashboard === true) {
                res.render('Admin/feedback', { profileImage, data, moment });
            }else if(result.dashboard === false) {
                res.render('Admin/permission_error',{profileImage});
            }
        });
    }).catch(error=>{
        console.log(error)
        res.send(error)
    })
}

exports.deleteFeedback=function(req,res,next){
    console.log(req.params.id)
    feedback.deleteOne({_id:mongoose.Types.ObjectId(req.params.id)}).then(data=>{
        if(data.deletedCount > 0){
            res.redirect('/feedbackList')
        }
        else{
            res.send({msg:'not deleted'})
 
        }
    }).catch(err=>{
        res.send({err})
    })
}

exports.deleteQuery=function(req,res,next){
    query.deleteOne({_id:mongoose.Types.ObjectId(req.params.id)}).then(data=>{
        if(data.deletedCount > 0){
            res.redirect('/queryList')
  
        }
        else{
            res.send({msg:'not deleted'})
 
        }
    }).catch(err=>{
        res.send({err})
    })
}


function getUserDetails(roleId,callback){
    var user = {};
    roles.find()
        .where('user_id').equals(roleId)
        .exec(function(err, roles) {
            if (err) { return next(err); }
            callback(roles[0]);
        });
  }
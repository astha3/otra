const mongoose = require('mongoose');
const user = require('../Models/users')
const role = require('../Models/role');
const Subject = require('../Models/subject');
const subject = require('../Models/subject');
const experience = require('../Models/experience');
const Experience = require('../Models/experience');
const Country = require('../Models/country');
const country = require('../Models/country');
const State = require('../Models/state');
const state = require('../Models/state');
const city = require('../Models/city');
const City = require('../Models/city');
const HearedFrom = require('../Models/hearedFrom');
const hearedFrom = require('../Models/hearedFrom');

const moment = require('moment')
const getClasses = require('../Models/class');
const schoolTypeModel = require('../Models/schoolType');
const roles = require('../Models/role')

exports.hearedFrom = function (req, res, next) {
    let profileImage = req.user.profileImage;
    res.locals = { title: 'hearedFrom' };
    hearedFrom.find().exec().then(data => {

        getUserDetails(req.user._id, function (result) {
            console.log(result)
            if (req.user.role === 1) {
                res.render('Admin/hearedFrom', { data, profileImage, moment })

            } else if (result.teacherSection === true) {
                res.render('Admin/hearedFrom', { data, profileImage, moment })
            } else if (result.teacherSection === false) {
                res.render('Admin/permission_error', { profileImage });
            }
        });


    })
}

exports.addHearedFrom = function (req, res, next) {
    let profileImage = req.user.profileImage;
    var hearedFrom = req.body.hearedFrom;
    console.log(new Date(), 'today Date')
    let saveHearedFrom = new HearedFrom({ name });
    saveHearedFrom.save().then(async doc => {

        console.log(doc)
        res.locals = { title: 'hearedFrom' };
        let data = await HearedFrom.find({}).exec()
        res.render('Admin/hearedFrom', { profileImage, moment, data })
    }).catch((err) => {
        next(new Error(err));
    })
}

exports.deleteHearedFrom = async function (req, res, next) {
    let profileImage = req.user.profileImage;
    try {
        var deletedOne = await HearedFrom.deleteOne({ _id: mongoose.Types.ObjectId(req.params.id) }).exec();
        res.locals = { title: 'hearedFrom' };
        let data = await HearedFrom.find({}).exec()
        res.render('Admin/hearedFrom', { profileImage, moment, data })
    }
    catch {
        next(err)
    }
}

exports.country = function (req, res, next) {
    let profileImage = req.user.profileImage;
    res.locals = { title: 'country' };
    country.find().exec().then(data => {
        var data1 = data;

        getUserDetails(req.user._id, function (result) {
            console.log(result)
            if (req.user.role === 1) {
                res.render('Admin/country', { data, profileImage, moment })
            }
            else if (result.teacherSection === true) {
                res.render('Admin/country', { data, profileImage, moment })
            }
            else if (result.teacherSection === false) {
                res.render('Admin/permission_error', { profileImage });
            }
        });
    })
}

exports.addCountry = function (req, res, next) {
    let profileImage = req.user.profileImage;
    var name = req.body.country;
    console.log(new Date(), 'today Date')
    let saveCountry = new Country({ name });
    saveCountry.save().then(async doc => {

        console.log(doc)
        res.locals = { title: 'country' };
        let data = await Country.find({}).exec()
        res.render('Admin/country', { profileImage, moment, data })
    }).catch((err) => {
        next(new Error(err));
    })
}

exports.deleteCountry = async function (req, res, next) {
    let profileImage = req.user.profileImage;
    try {
        var deletedOne = await Country.deleteOne({ _id: mongoose.Types.ObjectId(req.params.id) }).exec();
        res.locals = { title: 'country' };
        let data = await Country.find({}).exec()
        res.render('Admin/country', { profileImage, moment, data })
    }
    catch {
        next(err)
    }
}

exports.state = async function (req, res, next) {
    let profileImage = req.user.profileImage;
    res.locals = { title: 'state' };
    let data1 = await Country.find({}).exec();
    var data = await state.aggregate([
        {
            "$project": {
                "ca": "$$ROOT"
            }
        },
        {
            "$lookup": {
                from: "countries",
                localField: "ca.country_id",
                foreignField: "_id",
                as: "country"
            }
        },
        {
            "$unwind": {
                "path": "$country",
                "preserveNullAndEmptyArrays": true
            }
        },
        {
            $project: {
                "name": "$ca.name",
                "countryName": "$country.name",
                "createdAt": "$ca.createdAt",
            }
        }
    ])
    console.log(data, data);

    getUserDetails(req.user._id, function (result) {
        console.log(result)
        if (req.user.role === 1) {
            res.render('Admin/state', { data, profileImage, moment, data1, })
        } else if (result.teacherSection === true) {
            res.render('Admin/state', { data, profileImage, moment, data1 })
        } else if (result.teacherSection === false) {
            res.render('Admin/permission_error', { profileImage });
        }
    });
}

exports.addState = async function (req, res, next) {
    let profileImage = req.user.profileImage;
    var name = req.body.state;
    var country_id = req.body.country;
    console.log(req.body.state, req.body.country);

    var userVariable = {
        "name": req.body.state,
        "country_id": req.body.country
    }
    console.log(userVariable, "userVariable");
    let data1 = await Country.find({}).exec();
    let saveState = new State(userVariable);

    saveState.save().then(async doc => {

        console.log(doc)
        res.locals = { title: 'state' };
        var data = await state.aggregate([
            {
                "$project": {
                    "ca": "$$ROOT"
                }
            },
            {
                "$lookup": {
                    from: "countries",
                    localField: "ca.country_id",
                    foreignField: "_id",
                    as: "country"
                }
            },
            {
                "$unwind": {
                    "path": "$country",
                    "preserveNullAndEmptyArrays": true
                }
            },
            {
                $project: {
                    "name": "$ca.name",
                    "countryName": "$country.name",
                    "createdAt": "$ca.createdAt",
                }
            }
        ])
        res.render('Admin/state', { profileImage, moment, data, data1 })
    }).catch((err) => {
        next(new Error(err));
    })
}

exports.deleteState = async function (req, res, next) {
    let profileImage = req.user.profileImage;
    try {
        var deletedOne = await State.deleteOne({ _id: mongoose.Types.ObjectId(req.params.id) }).exec();
        res.locals = { title: 'state' };
        var data = await state.aggregate([
            {
                "$project": {
                    "ca": "$$ROOT"
                }
            },
            {
                "$lookup": {
                    from: "countries",
                    localField: "ca.country_id",
                    foreignField: "_id",
                    as: "country"
                }
            },
            {
                "$unwind": {
                    "path": "$country",
                    "preserveNullAndEmptyArrays": true
                }
            },
            {
                $project: {
                    "name": "$ca.name",
                    "countryName": "$country.name",
                    "createdAt": "$ca.createdAt",
                }
            }
        ])
        let data1 = await Country.find({}).exec();
        res.render('Admin/state', { profileImage, moment, data, data1 })
    }
    catch {
        next(err)
    }
}

exports.city = async function (req, res, next) {
    let profileImage = req.user.profileImage;
    res.locals = { title: 'city' };
    let data1 = await State.find({}).exec();
    var data = await city.aggregate([
        {
            "$project": {
                "ca": "$$ROOT"
            }
        },
        {
            "$lookup": {
                from: "states",
                localField: "ca.state_id",
                foreignField: "_id",
                as: "state"
            }
        },
        {
            "$unwind": {
                "path": "$state",
                "preserveNullAndEmptyArrays": true
            }
        },
        {
            $project: {
                "name": "$ca.name",
                "stateName": "$state.name",
                "createdAt": "$ca.createdAt",
            }
        }
    ])
    console.log(data, data);

    getUserDetails(req.user._id, function (result) {
        //  console.log(result)
        if (req.user.role === 1) {
            res.render('Admin/city', { data, profileImage, moment, data1 })

        } else if (result.teacherSection === true) {
            res.render('Admin/city', { data, profileImage, moment, data1 })
        } else if (result.teacherSection === false) {
            res.render('Admin/permission_error', { profileImage });
        }
    });



}

exports.addCity = async function (req, res, next) {
    let profileImage = req.user.profileImage;
    var userVariable = {
        "name": req.body.city,
        "state_id": req.body.state
    }
    console.log(userVariable, "userVariable");
    let data1 = await State.find({}).exec();
    let saveCity = new City(userVariable);
    saveCity.save().then(async doc => {

        console.log(doc)
        res.locals = { title: 'city' };
        var data = await city.aggregate([
            {
                "$project": {
                    "ca": "$$ROOT"
                }
            },
            {
                "$lookup": {
                    from: "states",
                    localField: "ca.state_id",
                    foreignField: "_id",
                    as: "state"
                }
            },
            {
                "$unwind": {
                    "path": "$state",
                    "preserveNullAndEmptyArrays": true
                }
            },
            {
                $project: {
                    "name": "$ca.name",
                    "stateName": "$state.name",
                    "createdAt": "$ca.createdAt",
                }
            }
        ])
        console.log(data, data);
        res.render('Admin/city', { profileImage, moment, data, data1 })
    }).catch((err) => {
        next(new Error(err));
    })
}

exports.deleteCity = async function (req, res, next) {
    let profileImage = req.user.profileImage;
    try {
        var deletedOne = await City.deleteOne({ _id: mongoose.Types.ObjectId(req.params.id) }).exec();
        res.locals = { title: 'city' };
        let data1 = await State.find({}).exec();
        var data = await city.aggregate([
            {
                "$project": {
                    "ca": "$$ROOT"
                }
            },
            {
                "$lookup": {
                    from: "states",
                    localField: "ca.state_id",
                    foreignField: "_id",
                    as: "state"
                }
            },
            {
                "$unwind": {
                    "path": "$state",
                    "preserveNullAndEmptyArrays": true
                }
            },
            {
                $project: {
                    "name": "$ca.name",
                    "stateName": "$state.name",
                    "createdAt": "$ca.createdAt",
                }
            }
        ])
        console.log(data, data);

        res.render('Admin/city', { profileImage, moment, data, data1 })
    }
    catch {
        next(err)
    }
}

exports.subject = async function (req, res, next) {
    let profileImage = req.user.profileImage;
    res.locals = { title: 'subject' };
    let data1 = await schoolTypeModel.find({}).exec();
    var data = await subject.aggregate([
        {
            "$project": {
                "ca": "$$ROOT"
            }
        },
        {
            "$lookup": {
                from: "school_types",
                localField: "ca.school_type",
                foreignField: "_id",
                as: "school_type"
            }
        },
        {
            "$unwind": {
                "path": "$school_type",
                "preserveNullAndEmptyArrays": true
            }
        },
        {
            $project: {
                "subject": "$ca.subject",
                "schoolType": "$school_type.type",
                "createdAt": "$ca.createdAt",
            }
        }
    ])
    console.log(data, data);

    getUserDetails(req.user._id, function (result) {
        console.log(result)
        if (req.user.role === 1) {
            res.render('Admin/subject', { data, profileImage, moment, data1 })

        } else if (result.teacherSection === true) {
            res.render('Admin/subject', { data, profileImage, moment, data1 })
        } else if (result.teacherSection === false) {
            res.render('Admin/permission_error', { profileImage });
        }
    });


}

exports.experience = function (req, res, next) {
    let profileImage = req.user.profileImage;
    res.locals = { title: 'experience' };
    experience.find().exec().then(data => {

        getUserDetails(req.user._id, function (result) {
            console.log(result)
            if (req.user.role === 1) {
                res.render('Admin/experience', { data, profileImage, moment })

            } else if (result.teacherSection === true) {
                res.render('Admin/experience', { data, profileImage, moment })
            } else if (result.teacherSection === false) {
                res.render('Admin/permission_error', { profileImage });
            }
        });


    })
}

exports.addSubject = async function (req, res, next) {
    let profileImage = req.user.profileImage;
    var userVariable = {
        "subject": req.body.subject,
        "school_type": req.body.schoolType
    }
    console.log(userVariable, "userVariable");
    let data1 = await schoolTypeModel.find({}).exec();
    let saveSubject = new Subject(userVariable);
    saveSubject.save().then(async doc => {
        console.log(doc, "doc");
        var data = await Subject.aggregate([
            {
                "$project": {
                    "ca": "$$ROOT"
                }
            },
            {
                "$lookup": {
                    from: "school_types",
                    localField: "ca.school_type",
                    foreignField: "_id",
                    as: "school_type"
                }
            },
            {
                "$unwind": {
                    "path": "$school_type",
                    "preserveNullAndEmptyArrays": true
                }
            },
            {
                $project: {
                    "subject": "$ca.subject",
                    "schoolType": "$school_type.type",
                    "createdAt": "$ca.createdAt",
                }
            }
        ])
        res.locals = { title: 'subject' };
        res.render('Admin/subject', { profileImage, moment, data, data1 });
    }).catch((err) => {
        next(new Error(err));
    })
}

exports.addExperience = function (req, res, next) {
    let profileImage = req.user.profileImage;
    var experience = req.body.experience;
    console.log(new Date(), 'today Date')
    let saveExperience = new Experience({ experience });
    saveExperience.save().then(async doc => {

        console.log(doc)
        res.locals = { title: 'experience' };
        let data = await Experience.find({}).exec()
        res.render('Admin/experience', { profileImage, moment, data })
    }).catch((err) => {
        next(new Error(err));
    })
}

exports.deleteSubject = async function (req, res, next) {
    let profileImage = req.user.profileImage;
    try {
        var deletedOne = await Subject.deleteOne({ _id: mongoose.Types.ObjectId(req.params.id) }).exec();
        res.locals = { title: 'subject' };
        let data1 = await schoolTypeModel.find({}).exec();
        var data = await subject.aggregate([
            {
                "$project": {
                    "ca": "$$ROOT"
                }
            },
            {
                "$lookup": {
                    from: "school_types",
                    localField: "ca.school_type",
                    foreignField: "_id",
                    as: "school_type"
                }
            },
            {
                "$unwind": {
                    "path": "$school_type",
                    "preserveNullAndEmptyArrays": true
                }
            },
            {
                $project: {
                    "subject": "$ca.subject",
                    "schoolType": "$school_type.type",
                    "createdAt": "$ca.createdAt",
                }
            }
        ])
        console.log(data, data);
        res.render('Admin/subject', { profileImage, moment, data, data1 })

    }
    catch {
        next(err)
    }
}

exports.deleteExperience = async function (req, res, next) {
    let profileImage = req.user.profileImage;
    try {
        var deletedOne = await Experience.deleteOne({ _id: mongoose.Types.ObjectId(req.params.id) }).exec();
        res.locals = { title: 'experience' };
        let data = await Experience.find({}).exec()
        res.render('Admin/experience', { profileImage, moment, data })

    }
    catch {
        next(err)
    }
}

exports.schoolType = async function (req, res, next) {
    let profileImage = req.user.profileImage;
    res.locals = { title: 'schoolTypes' };

    schoolTypeModel.find().exec(function (err, data) {
        getUserDetails(req.user._id, function (result) {
            console.log(result)
            if (req.user.role === 1) {
                if (err) {
                    console.log(err, 'error++++');
                    res.render('Admin/schoolType', { data: [], moment })

                }
                else {
                    console.log(data, 'data++++')
                    res.render('Admin/schoolType', { profileImage, data, moment })

                }
            } else if (result.teacherSection === true) {
                res.render('Admin/schoolType', { profileImage, data, moment })
            } else if (result.teacherSection === false) {
                res.render('Admin/permission_error', { profileImage });
            }
        });

    });
}

exports.addSchoolType = function (req, res, next) {
    let uservariable = {
        type: req.body.type
    }
    let addSchoolType = new schoolTypeModel(uservariable);
    addSchoolType.save().then(doc => {
        res.redirect('/schoolType')
    }).catch(err => [
        next(err)
    ])
}

exports.deleteSchoolType = async function (req, res, next) {
    try {
        var deletedOne = await schoolTypeModel.deleteOne({ _id: mongoose.Types.ObjectId(req.params.id) }).exec();
        res.redirect('/schoolType')
    }
    catch {
        next(err)
    }
}

exports.experience = function (req, res, next) {
    let profileImage = req.user.profileImage;
    res.locals = { title: 'experience' };
    experience.find().exec().then(data => {
        experience
        getUserDetails(req.user._id, function (result) {
            console.log(result)
            if (req.user.role === 1) {
                res.render('Admin/experience', { data, profileImage, moment })

            } else if (result.teacherSection === true) {
                res.render('Admin/experience', { data, profileImage, moment })
            } else if (result.teacherSection === false) {
                res.render('Admin/permission_error', { profileImage });
            }
        });


    })
}

function getUserDetails(roleId, callback) {
    var user = {};
    roles.find({ user_id: mongoose.Types.ObjectId(roleId) })
        .exec(function (err, roles) {
            if (err) { return next(err); }
            callback(roles[0]);
        });
}

var mongoose = require("mongoose");
var login_log = new mongoose.Schema({

    user_id: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'User'
    },
    device_token: {
        type: String,
        default: null
    },
    device_type: {
        type: String,
        default: null
    },
    status: {
        type: Boolean,
        default: true
    },
    // google_id: {
    //     type: String,
    //     default: null
    // },
    // facebook_id: {
    //     type: String,
    //     default: null
    // },
    // type: {
    //     type: Number,
    // }
},

    {
        timestamps: true // Saves createdAt and updatedAt as dates. createdAt will be our timestamp.
    });

module.exports = mongoose.model('login_log', login_log)
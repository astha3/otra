var mongoose = require('mongoose');
const experiences = require('./experience');
const subject = require('./subject');
var schema = mongoose.Schema;

var jobKeys = new schema({

    schoolName: {
        type: mongoose.Types.ObjectId,
        ref: 'User'
    },
    location: {
        type: String,
        required: true
    },
    salary: {
        type: String,
        required: true
    },
    jobType: {
        type: mongoose.Types.ObjectId,
        required: true,
        ref: 'school_type'
    },
    totalVacancy: {
        type: String,
        required: true
    },
    jobDescription: {
        type: String,
        required: true
    },
    experience: {
        type: mongoose.Types.ObjectId,
        required: true,
        ref: 'experiences'
    },
    schoolImage: {
        type: String
    },
    professionName: {
        type: String,
        required: true
    },
    address_lat_lng: {
        type: String,
        required: true
    },
    skills: {
        type: Array,
        required :true     
    }
    // skills:{
    //     type:mongoose.Types.ObjectId,
    //     required:true,
    //     ref:'subject'
    // }
},
    {
        timestamps: true     // Saves createdAt and updatedAt as dates. createdAt will be our timestamp.
    });

module.exports = mongoose.model("job", jobKeys)
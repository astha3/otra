/**
 * Created by Ajay Kumar Tripathi on 01/08/2019.
 */
 var mongoose = require("mongoose");
 var experiences = new mongoose.Schema({
    experience:{
        type:String,
        required:true
    }
 }, {
     timestamps: true // Saves createdAt and updatedAt as dates. createdAt will be our timestamp.
 });
 
 var experiences = module.exports = mongoose.model("experiences", experiences);
 module.exports = experiences;
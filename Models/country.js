/**
 * Created by Ajay Kumar Tripathi on 01/08/2019.
 */
var mongoose = require("mongoose");
var countries = new mongoose.Schema({
    name: {
        type: String,
        required: true
    },
    status: {
        type: String,
        default: null
    }
}, {
    timestamps: true // Saves createdAt and updatedAt as dates. createdAt will be our timestamp.
});

var countries = module.exports = mongoose.model("countries", countries);
module.exports = countries;
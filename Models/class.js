/**
 * Created by Ajay Kumar Tripathi on 01/08/2019.
 */
 var mongoose = require("mongoose");
 var classes = new mongoose.Schema({
    classname:{
        type:String,
        required:true
    }
 }, {
     timestamps: true // Saves createdAt and updatedAt as dates. createdAt will be our timestamp.
 });
 
 var classes = module.exports = mongoose.model("classes", classes);
 module.exports = classes;
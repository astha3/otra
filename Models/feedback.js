var mongoose = require('mongoose');
var schema = mongoose.Schema;

var feedback = new schema({
feedback:{
    type:String,
    default:null
},
user_id:{
    type:mongoose.Types.ObjectId,
    ref:'User'
},
school_id:{
    type:mongoose.Types.ObjectId,
    ref:'User'
},
rating:{
    type:Number,
    
}

  
},
    {
        timestamps: true     // Saves createdAt and updatedAt as dates. createdAt will be our timestamp.
    });

module.exports = mongoose.model("feedback", feedback)

// {

//   job_detail: {
//     _id: new ObjectId("618cdc08d194dbacd19cac31"),
//     schoolName: {
//       _id: new ObjectId("617699f482366378fa8d6dd0"),
//       username: 'gaurav kumar',
//       email: 'gaurav@maxtratechnologies.net',
//       phoneNumber: '7500974446',
//       password: '$2a$10$GF8l6GXhCoXda2og/eFZmeCSo/597q8rQz/PgP11QADKS5RYdEvuC',
//       profileImage: '16375593217181637559304726.jpg',
//       know_about_us: null,
//       country_code: null,
//       address: 'asdfghjk',
//       role: 2,
//       status: true,
//       approve: true,
//       createdAt: 2021-10-25T11:50:12.487Z,
//       updatedAt: 2021-11-22T08:36:28.016Z,
//       __v: 0
//     },
//     location: '82, C Block, Sector 56, Noida, Uttar Pradesh 201301, India',
//     salary: '2.5 lakhs',
//     jobType: 'type 2',
//     totalVacancy: '1',
//     jobDescription: 'asdfghjk',
//     experience: {
//       _id: new ObjectId("6135fedafff6fb077864e7f0"),
//       experience: 'Trainee'
//     },
//     professionName: 'title 3',
//     address_lat_lng: '28.6009464,77.343917',
//     skills: {
//       _id: new ObjectId("613b048f019fb8e887360f2c"),
//       subject: 'Chemistry'
//     },
 
//   },
//   job_status: 0
// }
/**
 * Created by Ajay Kumar Tripathi on 01/08/2019.
 */
 var mongoose = require("mongoose");
 var roles = new mongoose.Schema({
    rolename:{
        type:String,
        required:true
    }
 }, {
     timestamps: true // Saves createdAt and updatedAt as dates. createdAt will be our timestamp.
 });
 
 var roles = module.exports = mongoose.model("user_roles", roles);
 module.exports = roles;
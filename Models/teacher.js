
var mongoose = require("mongoose");
var teacher = new mongoose.Schema({
    teacher_id: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'User',
        required: true
    },
    experience: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'experiences',
        default: null
    },
    resume: {
        type: String,
        default: null
    },
    school_type: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'school_type',
        default: null
    },
    subject: {
        type: String,
        required: true
      
    },
    skills: {
        type: String,
        default: null
    },
    education_detail: {
        type: String,
        default: null
    },
    openToWork: {
        type: Boolean,
        default: true
    }
}, {
    timestamps: true // Saves createdAt and updatedAt as dates. createdAt will be our timestamp.
});

var roles = module.exports = mongoose.model("teacher", teacher);
module.exports = roles;
var mongoose = require('mongoose');
var schema = mongoose.Schema;

var query = new schema({
query:{
    type:String,
    default:null
},
user_id:{
    type:mongoose.Types.ObjectId,
    ref:'User'
},

  
},
    {
        timestamps: true     // Saves createdAt and updatedAt as dates. createdAt will be our timestamp.
    });

module.exports = mongoose.model("query", query)
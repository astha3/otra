var mongoose = require('mongoose');
var schema = mongoose.Schema;

var job_status = new schema({
job_id:{
type:mongoose.Types.ObjectId,
ref:'jobs'
},
applicant_id:{
    type:mongoose.Types.ObjectId,
    ref:'User'
},
job_status:{
    type:Number,
    required:true
},
school_id:{
    type:mongoose.Types.ObjectId,
    required:true
},
application_id:{
    type:mongoose.Types.ObjectId,//applied job id
    ref:'jobApplied',
    required:true
}

  
},
    {
        timestamps: true     // Saves createdAt and updatedAt as dates. createdAt will be our timestamp.
    });

module.exports = mongoose.model("job_status", job_status)
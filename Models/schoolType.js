/**
 * Created by Ajay Kumar Tripathi on 01/08/2019.
 */
 var mongoose = require("mongoose");
 var school_type = new mongoose.Schema({
    type:{
        type:String,
        required:true
    }
 }, {
     timestamps: true   // Saves createdAt and updatedAt as dates. createdAt will be our timestamp.
 });
 
 var school_type = module.exports = mongoose.model("school_type", school_type);
 module.exports = school_type;
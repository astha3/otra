/**
 * Created by Ajay Kumar Tripathi on 01/08/2019.
 */
 var mongoose = require("mongoose");
 var rolesSchema = new mongoose.Schema({
     user_id:{
        type: mongoose.Schema.Types.ObjectId,
        ref: 'User'
     },
     dashboard :{ type:Boolean,default:false },
     subAdmin:{ type:Boolean,default:false },
     customerSupport:{ type:Boolean,default:false },
     teacher:{ type:Boolean,default:false },
     school:{type:Boolean,default:false},
     teacherSection:{type:Boolean,default:false},
     subscriptionPlan:{type:Boolean,default:false},
     updatedby:{
        type: mongoose.Schema.Types.ObjectId,
        ref: 'User'
     },
 }, {
     timestamps: true // Saves createdAt and updatedAt as dates. createdAt will be our timestamp.
 });
 
 var roles = module.exports = mongoose.model("subadmin_roles", rolesSchema);
 module.exports = roles;
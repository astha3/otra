/**
 * Created by Ajay Kumar Tripathi on 01/08/2019.
 */
 var mongoose = require("mongoose");
 var subject = new mongoose.Schema({
    subject:{
        type:String,
        required:true
    },
    school_type:{
        type: mongoose.Schema.Types.ObjectId,
        ref: 'school_type',
        default: null
    }
 }, {
     timestamps: true // Saves createdAt and updatedAt as dates. createdAt will be our timestamp.
 });
 
 var subject = module.exports = mongoose.model("subject", subject);
 module.exports = subject;

 var mongoose = require("mongoose");
 
 var states = new mongoose.Schema({
    name:{
        type:String,
        required:true
    },
    country_id:{
        type: mongoose.Schema.Types.ObjectId,
        ref: 'country',
        default: null
    },
    status: {
        type: String,
        default: null
    }
 }, {
     timestamps: true // Saves createdAt and updatedAt as dates. createdAt will be our timestamp.
 });
 
 var states = module.exports = mongoose.model("state", states);
 module.exports = states;
/**
 * Created by Ajay Kumar Tripathi on 01/08/2019.
 */
 var mongoose = require("mongoose");
 var subscription = new mongoose.Schema({
    subscription_name:{
        type:String,
        required:true
    },
    validity:{
        type:String
    },
    charge:{
        type:Number
    },
    description:{
        type:String
    }
 }, {
     timestamps: true // Saves createdAt and updatedAt as dates. createdAt will be our timestamp.
 });
 
 var subscription = module.exports = mongoose.model("subscription", subscription);
 module.exports = subscription;
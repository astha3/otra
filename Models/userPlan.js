var mongoose = require('mongoose');
var schema = mongoose.Schema;

var supportKeys = new schema({

    user_id: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'user'
    },
    plan_id: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'subscription'
    },
    transectionId:{
        type: String,
        default: null
    },
    paymentMode: {
        type: String,
        enum: ["Online"],
        default: "Online"
    },
    // subscriptionDate:{
    //     type: String,
    //     default: null
    // },
    // expireyDate:{
    //     type: String,
    //     default: null
    // },

},
    {
        timestamps: true      // Saves createdAt and updatedAt as dates. createdAt will be our timestamp.
    });

module.exports = mongoose.model("userPlan", supportKeys);
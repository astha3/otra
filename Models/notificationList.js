var mongoose = require('mongoose');
var schema = mongoose.Schema;

var notifications = new schema({
    user_id:{
        type:mongoose.Types.ObjectId,
        ref:"User"
    },
    application_id:{
        type: mongoose.Schema.Types.ObjectId,
        ref: 'jobApplied'
    },
    job_id:{
        type: mongoose.Schema.Types.ObjectId,
        ref: 'jobs'
    },
    status:{
        type:Number,//0:not applied,1:apply,2:seen,3:rejected,4:accepted
    }
},
    {
        timestamps: true     // Saves createdAt and updatedAt as dates. createdAt will be our timestamp.
    });

module.exports = mongoose.model("notifications", notifications)
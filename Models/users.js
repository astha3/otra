
var mongoose = require("mongoose");
var userSchema = new mongoose.Schema({
    username: { type: String,  default:null },
    email: { type: String,  default:null },
    phoneNumber: { type: String,  default:null },
    password: { type: String,  default:null },
    schoolNumber: {type: String, default:null},
    profileImage: { type: String, default: null },
    know_about_us: { type: String, default: null },
    country_code: { type: String, default: null },
    address: { type: String, default: null },
    role: { type: Number, required: true },      // admin= 1, School= 2, Teacher= 3 , 
    status: { type: Boolean, default: true },
    approve: { type: String, default: false },
    country: { type: mongoose.Schema.Types.ObjectId, ref: 'country', default: null },
    state: { type: mongoose.Schema.Types.ObjectId, ref: 'state', default: null },
    city: { type: mongoose.Schema.Types.ObjectId, ref: 'city', default: null },

    device_type: { type: String, default: null },
    device_token: { type: String, default: null },


    google_id: {
        type: String,
        default: null
    },
    facebook_id: {
        type: String,
        default: null
    },
    type: {
        type: Number,
       default: null
    },
    
    isSubscribe: {
        type: Boolean,
        default: false
    }



},
    {
        timestamps: true // Saves createdAt and updatedAt as dates. createdAt will be our timestamp.
    });

userSchema.methods.name = function () {
    return this.displayName || this.username;
};

var bcrypt = require("bcrypt-nodejs");
const experiences = require("./experience");
var SALT_FACTOR = 10;

var noop = function () { };
userSchema.pre("save", function (done) {
    var user = this;
    if (!user.isModified("password")) {
        return done();
    }
    bcrypt.genSalt(SALT_FACTOR, function (err, salt) {
        if (err) { return done(err); }
        bcrypt.hash(user.password, salt, noop, function (err, hashedPassword) {
            if (err) { return done(err); }
            user.password = hashedPassword;
            done();
        });
    });
});

userSchema.methods.checkPassword = function (guess, done) {
    bcrypt.compare(guess, this.password, function (err, isMatch) {
        done(err, isMatch);
    });
};

var User = module.exports = mongoose.model("User", userSchema);

module.exports.getUserById = function (id, callback) {
    User.findById(id, callback);
};
module.exports.getUserByUsername = function (username, callback) {
    if (isNaN(username)) {
        var query = { email: username };
    } else {
        var query = { phoneNumber: username };
    }
    // console.log(query); 
    // var query = {username:username};
    User.findOne(query, callback);
};
module.exports.comparePassword = function (candidatePassword, hash, callback) {
    bcrypt.compare(candidatePassword, hash, function (err, isMatch) {
        console.log("user model line no 69 isMatch", isMatch)
        callback(null, isMatch);
    });
};

module.exports = User;
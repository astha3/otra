var mongoose = require('mongoose');
var schema = mongoose.Schema;

var invitation = new schema({
message:{
    type:String,
    required:true
},
user_id:{
    type:mongoose.Types.ObjectId,
    ref:'User'
},
school_id:{
    type:mongoose.Types.ObjectId,
    ref:'User'
},
title:{
    type:String,
    required:true
},
application_id:{
    type:mongoose.Types.ObjectId,
    ref:'applied_for_jobs'
}
  
},
    {
        timestamps: true     // Saves createdAt and updatedAt as dates. createdAt will be our timestamp.
    });

module.exports = mongoose.model("invitationMessages", invitation)
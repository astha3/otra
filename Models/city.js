/**
 * Created by Ajay Kumar Tripathi on 01/08/2019.
 */
 var mongoose = require("mongoose");
 var cities = new mongoose.Schema({
    name:{
        type:String,
        required:true
    },
    state_id:{
        type: mongoose.Schema.Types.ObjectId,
        ref: 'country',
        default: null
    },
    status: {
        type: String,
        default: null
    }
 }, {
     timestamps: true // Saves createdAt and updatedAt as dates. createdAt will be our timestamp.
 });
 
 var cities = module.exports = mongoose.model("cities", cities);
 module.exports = cities;
var mongoose = require('mongoose');
var schema = mongoose.Schema;

var feedback = new schema({
feedback:{
    type:String,
    default:null
},
user_id:{
    type:mongoose.Types.ObjectId,
    ref:'User'
},

  
},
    {
        timestamps: true     // Saves createdAt and updatedAt as dates. createdAt will be our timestamp.
    });

module.exports = mongoose.model("feedback", feedback)
var app = require('express')();
var express = require('express');
var path = require('path');
var http = require('http').Server(app);
var bCrypt = require('bcryptjs');
var bodyParser = require('body-parser');
var router = require('./router.js');
var Authrouter = require('./Authrouter.js');
const mongoose=require('mongoose')
var session = require("express-session");

const flash = require('express-flash');
var passport = require('passport');

// Access public folder from root
app.use('/public', express.static('public'));
app.get('/layouts/', function(req, res) {
  res.render('view');
});
app.use(session({
  secret: "kgkhghhj",
  resave: false,
  saveUninitialized: true,
  cookie: {
    maxAge: 3600000,
    //secure : true
   }
}));
app.use(flash())
app.use(bodyParser.urlencoded({extended: true}));
app.use(bodyParser.json({ type: 'application/json' }));

app.use(passport.initialize());
app.use(passport.session());
//Flashing
//app.use(require('connect-flash-plus')());
app.use(function (req, res, next) {
  res.locals.messages = require('express-messages')(req, res);
  next();
});

//End Flashing


mongoose.connect("mongodb://localhost:27017/otra",{ useNewUrlParser: true });


// Add Authentication Route file with app
app.use('/', Authrouter); 

app.get('/callbackPayment', (req, res) => {
  return res.send({ status : 'success', message: "Hello.......!" })
})
//For set layouts of html view
var expressLayouts = require('express-ejs-layouts');
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'ejs');
app.use(expressLayouts);
app.use('/upload', express.static(__dirname + '/upload'));
app.use(function (req, res, next) {

  // Website you wish to allow to connect
  res.setHeader('Access-Control-Allow-Origin', '*');

  // Request methods you wish to allow
  res.setHeader('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, PATCH, DELETE');

  // Request headers you wish to allow
  res.header("Access-Control-Allow-Headers", "Access-Control-Allow-Headers, Origin, X-Requested-With, Content-Type, Accept, Authorization");
  // Set to true if you need the website to include cookies in the requests sent
  // to the API (e.g. in case you use sessions)
  res.setHeader('Access-Control-Allow-Credentials', true);

  // Pass to next layer of middleware
  next();
});



app.options('*', function (req,res) { res.sendStatus(200); });



// Add Route file with app
app.use('/', router); 

http.listen(9000, function(){
  console.log('listening on *:9000');
});
